#pragma once 

#include <stdbool.h>



typedef char *(*test_function)();

int test_global_fail_counter;

#define TEST_STATE(ret) (ret ? true : (++test_global_fail_counter && false))

#define TEST_RETURN(testname, ret) (TEST_STATE(ret) ? "[ OK ] " testname "\n": "[FAIL] " testname "\n")