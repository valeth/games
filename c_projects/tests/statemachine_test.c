#include "test.h"
#include "statemachine.h"
#include "app.h"



int main(int argc, char **argv)
{
	int ret = 0;
	App app = appCreate("State Machine", 640, 480);
	
	if (app) {
		ret = appRun(app, argc, argv);
	}
	
	appDestroy(app);
	
	return ret;
}