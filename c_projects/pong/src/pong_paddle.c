#include "pong_paddle.h"
#include "debug.h"

#include <stdlib.h>


struct pong_paddle {
	double x;
	double y;
	int width;
	int height;
	double y_vel;

	RenderManager rm;
	RenderObject rect;

	ColRect collision;
	bool top_collided;
	bool bot_collided;

	PongPaddleDirection moving;
};


PongPaddle pongPaddleCreate(RenderManager rm, int x, int y, int w, int h)
{
	bool success = true;
	PongPaddle new_paddle = malloc(sizeof(*new_paddle));

	if (new_paddle) {
		new_paddle->x = x;
		new_paddle->y = y;
		new_paddle->width = w;
		new_paddle->height = h;
		new_paddle->y_vel = 640.0;
		new_paddle->moving = PONG_PADDLE_STOP;
		new_paddle->top_collided = false;
		new_paddle->bot_collided = false;
		new_paddle->rm = rm;
	} else {
		success = false;
		LOG_ERROR("Failed to create paddle!");
	}

	if (success) {
		RenderObject new_rect = renderObjectCreate(rm->renderer);

		if (new_rect) {
			renderObjectSetColor(new_rect, 0xFF, 0xFF, 0xFF, 0xFF);
			renderObjectLoadTextureRect(new_rect, w, h);
			renderObjectSetPos(new_rect, x, y);
		} else {
			success = false;
			LOG_ERROR("Failed to create paddle rect!");
		}

		new_paddle->rect = new_rect;
	}

	if (success) {
		ColRect new_collision = colCreateRectangle(x, y, w, h);

		if (!new_collision) {
			success = false;
			LOG_ERROR("Failed to set collision!");
		}

		new_paddle->collision = new_collision;
	}

	if (!success) {
		pongPaddleDestroy(new_paddle);
		new_paddle = NULL;
	}

	return new_paddle;
}

void pongPaddleDestroy(PongPaddle paddle)
{
	if (paddle) {
		if (paddle->rect) {
			renderObjectDestroy(paddle->rect);
			paddle->rect = NULL;
		}

		if (paddle->collision) {
			colObjectDestroy((ColObject)paddle->collision);
			paddle->collision = NULL;
		}

		free(paddle);
	}
}


bool pongPaddleCollideWithBorder(PongPaddle paddle, SDL_Rect border)
{
	ColRect top_border = colCreateRectangle(border.x, 0, border.w, 0);
	ColRect bot_border = colCreateRectangle(border.x, border.h, border.w, 0);

	paddle->top_collided = CHECK_COLLISION(paddle->collision, top_border);
	paddle->bot_collided = CHECK_COLLISION(paddle->collision, bot_border);

	colObjectDestroy((ColObject)top_border);
	colObjectDestroy((ColObject)bot_border);

	return paddle->top_collided || paddle->bot_collided;
}

void pongPaddleMove(PongPaddle paddle, double step)
{
	double next_y = paddle->y;

	switch (paddle->moving) {
	case PONG_PADDLE_DOWN:
		if (!paddle->bot_collided) {
			next_y += (paddle->y_vel * step) * PONG_PADDLE_DOWN;
		}

		break;

	case PONG_PADDLE_UP:
		if (!paddle->top_collided) {
			next_y += (paddle->y_vel * step) * PONG_PADDLE_UP;
		}

		break;
	}

	pongPaddleSetPos(paddle, paddle->x, next_y);
}

ColObject pongPaddleGetCollider(PongPaddle paddle)
{
	return (ColObject)paddle->collision;
}

void pongPaddleRender(PongPaddle paddle)
{
	rendermanagerEnqueue(paddle->rm, paddle->rect);
}


void pongPaddleSetPos(PongPaddle paddle, double x, double y)
{
	paddle->x = x;
	paddle->y = y;

	renderObjectSetPos(paddle->rect, x, y);
	colObjectSetPos((ColObject)paddle->collision, x, y);
}

void pongPaddleSetViewport(PongPaddle paddle, SDL_Rect *viewport)
{
	renderObjectSetViewport(paddle->rect, viewport);
}

void pongPaddleSetDirection(PongPaddle paddle, PongPaddleDirection direction)
{
	paddle->moving = direction;
}