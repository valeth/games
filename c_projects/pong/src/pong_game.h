#pragma once

#include "rendermanager.h"
#include "statemachine.h"
#include "pong_player.h"
#include "pong_ball.h"

#include <SDL.h>


struct pong_game;

typedef struct pong_game *PongGame;


PongGame pongCreateGame(RenderManager rm, int x, int y, int w, int h);

void pongDestroyGame(PongGame game);


void pongResetGame(PongGame game);

void pongGameAI(PongPlayer player, PongBall ball, int rand);


int pongEventHandler(StateHandler sth, SDL_Event event);

int pongRenderHandler(StateHandler sth, RenderManager rm);

int pongLogicHandler(StateHandler sth);