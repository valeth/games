#include "pong_score.h"
#include "debug.h"

#include <stdlib.h>
#include <stdio.h>


static RenderObject pongScorePlayerCreate(ScoreBoard score, char *score_str);

static void pongScorePlayerDestroy(RenderObject player_score);


struct score_board {
	SDL_Rect board;
	RenderManager rm;
	TTF_Font *score_font;

	RenderObject score_line;
	RenderObject player1_score;
	RenderObject player2_score;
};


ScoreBoard pongScoreCreate(RenderManager rm, int x, int y, int w, int  h)
{
	bool success = true;
	ScoreBoard new_sb = malloc(sizeof(*new_sb));

	// set board dimensions
	if (new_sb) {
		new_sb->board = (SDL_Rect){.x = x, .y = y, .w = w, .h = h};
		new_sb->rm = rm;
	} else {
		success = false;
	}

	// load font
	if (success) {
		new_sb->score_font = TTF_OpenFont("res/liberation_mono_regular.ttf", 56);

		if (!new_sb->score_font) {
			LOG_ERROR("Failed to open front for scoreboard! SDL_ttf Error: %s", TTF_GetError());
			success = false;
		}
	}

	// create line on the bottom of the score field
	if (success) {
		RenderObject score_line = renderObjectCreate(rm->renderer);

		if (score_line) {
			int x_pos = new_sb->board.x;
			int y_pos = new_sb->board.y + new_sb->board.h - 10;
			int width = new_sb->board.w;

			renderObjectSetColor(score_line, 0xFF, 0xFF, 0xFF, 0xFF);
			renderObjectLoadTextureRect(score_line, width, 10);
			renderObjectSetPos(score_line, x_pos, y_pos);
			renderObjectSetViewport(score_line, &new_sb->board);

			new_sb->score_line = score_line;
		} else {
			success = false;
		}
	}

	// create the score counters for player 1 and 2
	if (success) {
		RenderObject player1_score = pongScorePlayerCreate(new_sb, "0");

		if (player1_score) {
			int x_pos = new_sb->board.x;
			int y_pos = new_sb->board.y;

			renderObjectSetPos(player1_score, x_pos, y_pos);
			renderObjectSetViewport(player1_score, &new_sb->board);
		} else {
			LOG_ERROR("Failed to create score text for player 1");
			success = false;
		}

		new_sb->player1_score = player1_score;
	}

	if (success) {
		RenderObject player2_score = pongScorePlayerCreate(new_sb, "0");

		if (player2_score) {
			int x_pos = new_sb->board.x + new_sb->board.w - player2_score->dim.w;
			int y_pos = new_sb->board.y;

			renderObjectSetPos(player2_score, x_pos, y_pos);
			renderObjectSetViewport(player2_score, &new_sb->board);
		} else {
			LOG_ERROR("Failed to create score text for player 2");
			success = false;
		}

		new_sb->player2_score = player2_score;
	}

	// clean up on error
	if (!success) {
		pongScoreDestroy(new_sb);
		new_sb = NULL;

		LOG_ERROR("Failed to create scoreboard!");
	}

	return new_sb;
}

void pongScoreDestroy(ScoreBoard score)
{
	if (score) {
		if (score->score_line) {
			renderObjectDestroy(score->score_line);
			score->score_line = NULL;
		}

		if (score->score_font) {
			TTF_CloseFont(score->score_font);
			score->score_font = NULL;
		}

		pongScorePlayerDestroy(score->player1_score);
		score->player1_score = NULL;

		pongScorePlayerDestroy(score->player2_score);
		score->player2_score = NULL;

		free(score);
	}
}

static RenderObject pongScorePlayerCreate(ScoreBoard score, char *score_str)
{
	RenderObject new_pscore = renderObjectCreate(score->rm->renderer);

	if (new_pscore) {
		renderObjectSetColor(new_pscore, 0xFF, 0xFF, 0xFF, 0xFF);
		renderObjectLoadTextureFromText(new_pscore, score_str, score->score_font);
	}

	return new_pscore;
}

static void pongScorePlayerDestroy(RenderObject player_score)
{
	if (player_score) {
		renderObjectDestroy(player_score);
	}
}


void pongScoreRender(ScoreBoard score)
{
	rendermanagerEnqueue(score->rm, score->score_line);
	rendermanagerEnqueue(score->rm, score->player1_score);
	rendermanagerEnqueue(score->rm, score->player2_score);

}

void pongScoreUpdate(ScoreBoard score, PongPlayer player1, PongPlayer player2)
{
	char player1_score[64];
	sprintf(player1_score, "%d",pongPlayerGetScore(player1));
	RenderObject new_p1score = pongScorePlayerCreate(score, player1_score);

	if (new_p1score) {
		int x_pos = score->board.x;
		int y_pos = score->board.y;

		renderObjectSetPos(new_p1score, x_pos, y_pos);
		renderObjectSetViewport(new_p1score, &score->board);

		pongScorePlayerDestroy(score->player1_score);
		score->player1_score = new_p1score;
	} else {
		LOG_ERROR("Failed to update player 1 score!");
	}

	char player2_score[64];
	sprintf(player2_score, "%d", pongPlayerGetScore(player2));
	RenderObject new_p2score = pongScorePlayerCreate(score, player2_score);

	if (new_p2score) {
		int x_pos = score->board.x + score->board.w - new_p2score->dim.w;
		int y_pos = score->board.y;

		renderObjectSetPos(new_p2score, x_pos, y_pos);
		renderObjectSetViewport(new_p2score, &score->board);

		pongScorePlayerDestroy(score->player2_score);
		score->player2_score = new_p2score;
	} else {
		LOG_ERROR("Failed to update player 1 score!");
	}
}


SDL_Rect pongScoreGetBoard(ScoreBoard score)
{
	return score->board;
}