#include "window.h"

#include <stdlib.h>
#include <stdio.h>



Window windowCreate(const char *title, int x, int y, int width, int height, Uint32 flags)
{
	bool success = true;
	Window new_window = malloc(sizeof(*new_window));
	
	if (!new_window) {
		fprintf(stderr, "Failed to allocate memory for new window!\n");
		success = false;
	} else {
		if (x < 0)
			new_window->x = SDL_WINDOWPOS_UNDEFINED;
		else
			new_window->x = x;
		
		if (y < 0)
			new_window->y = SDL_WINDOWPOS_UNDEFINED;
		else
			new_window->y = y;
	
		new_window->title = title;
		new_window->width = width;
		new_window->height = height;
		new_window->flags = flags;
	}
		
	// create the window
	if (success) {
		new_window->window = SDL_CreateWindow(new_window->title,
						      new_window->x,
						      new_window->y,
						      new_window->width,
						      new_window->height,
						      new_window->flags);
		
		if (!new_window->window) {
			fprintf(stderr, "Failed to create window, SDL Error: %s\n", SDL_GetError());
			success = false;
		} 
	}
	
	// cleanup on error
	if (!success) {
		windowDestroy(new_window);
		new_window = NULL;
	}
	
	return new_window;
}


bool windowDestroy(Window window)
{
	bool success = true;
	
	if (window) {
		if (window->window) {
			SDL_DestroyWindow(window->window);
			window->window = NULL;
		}
		
		free(window);
	} else {
		success = false;
	}
	
	return success;
}