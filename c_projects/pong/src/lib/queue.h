#pragma once

#include "list.h"

#include <stdbool.h>


struct queue;

typedef struct queue *Queue;


Queue queueCreate(unsigned int size);

bool queueDestroy(Queue queue);

bool queueIsEmpty(Queue queue);

bool queueIsFull(Queue queue);

bool queuePut(Queue queue, void *data);

void *queueGet(Queue queue);

void *queueFront(Queue queue);
