#include "graph.h"
#include "debug.h"

#include <stdlib.h>
#include <stdio.h>


struct vertex {
	void *data;
	int index;

	bool visited;
	List connections;
};

struct graph {
	GraphType type;
	List adj_list;
};


static Vertex graphVertexCreate(int id, void *data);
static bool graphVertexDestroy(Vertex vertex);
static Vertex graphVertexGet(List list, int id);
static void graphResetVisitedStatus(List adj_list);
static bool graphVerticesConnected(List path, Vertex src, Vertex dest);


static Vertex graphVertexCreate(int id, void *data)
{
	Vertex new_vertex = malloc(sizeof(*new_vertex));

	if (new_vertex) {
		new_vertex->connections = listCreate();

		if (new_vertex->connections) {
			new_vertex->index = id;
			new_vertex->data = data;
			new_vertex->visited = false;
		} else {
			LOG_ERROR("Failed to create new Vertex");
			free(new_vertex);
			new_vertex = NULL;
		}
	}

	return new_vertex;
}

static bool graphVertexDestroy(Vertex vertex)
{
	bool success = true;

	if (vertex) {
		listDestroy(vertex->connections);
		vertex->connections = NULL;

		free(vertex);
	}

	return success;
}

Graph graphCreate(int size, GraphType type)
{
	Graph new_graph = malloc(sizeof(*new_graph));

	if (new_graph) {
		new_graph->type = type;
		new_graph->adj_list = listCreate();

		if (new_graph->adj_list)
			for (int i = 0; i < size; ++i)
				graphAddVertex(new_graph, i, NULL);
	} else {
		LOG_ERROR("Failed to create graph!");
	}

	return new_graph;
}

bool graphDestroy(Graph graph)
{
	bool success = true;

	if (graph) {
		if (graph->adj_list) {
			LIST_FOREACH(graph->adj_list, current_node)
				graphVertexDestroy((Vertex)listGetData(current_node));

			listDestroy(graph->adj_list);
		}

		free(graph);
	} else {
		LOG_ERROR("Failed to delete graph!");
		success = false;
	}

	return success;
}


static void graphResetVisitedStatus(List adj_list)
{
	LIST_FOREACH(adj_list, adj_node) {
		Vertex cur_vertex = (Vertex)listGetData(adj_node);
		cur_vertex->visited = false;
	}
}

static bool graphVerticesConnected(List path, Vertex src, Vertex dest)
{
	// check if src connects to dest directly
	bool connects = graphVertexGet(src->connections, dest->index);

	if (!connects) {
		src->visited = true;

		// check if we can reach dest through any of our connections
		LIST_FOREACH (src->connections, conn) {
			Vertex next_vertex = (Vertex)listGetData(conn);

			if (!next_vertex->visited)
				connects = graphVerticesConnected(path, next_vertex, dest);

			if (connects) {
				break;
			}
		}
	} else if (path) {
		listUnshift(path, src);
	}

	return connects;
}

bool graphAddVertex(Graph graph, int id, void *data)
{
	bool success = true;

	/* check if vertex id is already in the list */
	if (id >= listLen(graph->adj_list)) {
		Vertex new_vertex = graphVertexCreate(id, data);

		if (new_vertex) {
			success = listPush(graph->adj_list, new_vertex);
		} else {
			success = false;
		}
	} else {
		LOG_ERROR("Failed to add Vertex %d!", id);
		success = false;
	}

	return success;
}

bool graphAddEdge(Graph graph, int v_id, int w_id)
{
	bool success = true;
	Vertex v_vertex = NULL;
	Vertex w_vertex = NULL;

	// check if the id can be in the list
	if (v_id >= listLen(graph->adj_list)) {
		success = false;
	}

	if (w_id >= listLen(graph->adj_list)) {
		success = false;
	}

	if (success) {
		v_vertex = graphVertexGet(graph->adj_list, v_id);
		w_vertex = graphVertexGet(graph->adj_list, w_id);
	}

	if (v_vertex && w_vertex) {
		if (!graphVertexGet(v_vertex->connections, w_id)) {
			success = listPush(v_vertex->connections, w_vertex);
		} else {
			success = false;
		}

		if (graph->type == GRAPH_UNDIRECTED) {
			if (!graphVertexGet(w_vertex->connections, v_id)) {
				success = listPush(w_vertex->connections, v_vertex);
			} else {
				success = false;
			}
		}
	} else {
		success = false;
	}

	return success;
}

bool graphIsConnected(Graph graph, int v_id, int w_id)
{
	Vertex v_vertex = graphVertexGet(graph->adj_list, v_id);
	Vertex w_vertex = graphVertexGet(graph->adj_list, w_id);

	bool success = graphVerticesConnected(NULL, v_vertex, w_vertex);

	graphResetVisitedStatus(graph->adj_list);

	return success;
}


// accessors
static Vertex graphVertexGet(List list, int id)
{
	Vertex found_vertex = NULL;

	if (list) {
		LIST_FOREACH(list, current_node) {
			Vertex cur_vertex = (Vertex)listGetData(current_node);

			if (cur_vertex->index == id) {
				found_vertex = cur_vertex;
				break;
			}
		}
	}

	return found_vertex;
}

List graphGetConnectionPath(Graph graph, int v_id, int w_id)
{
	bool success = true;

	Vertex v_vertex = graphVertexGet(graph->adj_list, v_id);
	Vertex w_vertex = graphVertexGet(graph->adj_list, w_id);

	List path = listCreate();

	if (path) {
		success = graphVerticesConnected(path, v_vertex, w_vertex);
	} else {
		success = false;
	}

	if (!success) {
	} else {
		listPush(path, w_vertex);
	}

	return path;
}

void *graphGetData(Graph graph, int id)
{
	Vertex tmp_vertex = graphVertexGet(graph->adj_list, id);

	return tmp_vertex->data;
}


// mutators
bool graphSetData(Graph graph, int v_id, void *data)
{
	bool success = true;

	Vertex set_vertex = graphVertexGet(graph->adj_list, v_id);

	if (set_vertex) {
		set_vertex->data = data;
	} else {
		success = false;
	}

	return success;
}
