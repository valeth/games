/*!
 * \file render_object.h
 * \author Patrick Auernig
 * \date 08.09.2015
 * \brief render object that holds render information
 */

#pragma once

#include "window.h"

#include <stdbool.h>

#include <SDL.h>
#include <SDL_ttf.h>



/*! \brief render object type, set after creation */
typedef enum RENDER_OBJECT_TYPE {
	RENDER_OBJECT_TYPE_NONE,
	RENDER_OBJECT_TYPE_IMAGE,
	RENDER_OBJECT_TYPE_GEOMETRY_RECTANGLE,
	RENDER_OBJECT_TYPE_GEOMETRY_TIANGLE,
	RENDER_OBJECT_TYPE_GEOMETRY_CIRCLE,
} RENDER_OBJECT_TYPE;


/*! \brief object that holds information for the SDL renderer */
struct render_object {
	SDL_Texture *texture;		//!< the texture for the renderer
	SDL_Rect dim;			//!< the dimensions of the texture
	SDL_Rect *viewport;
	RENDER_OBJECT_TYPE type;	//!< the texture type
	
	SDL_Color color_key;		//!< the color key for transparency
	SDL_Color color;		//!< the render color
	
	SDL_Rect *clip;			//!< the clip rectangle
	double angle;			//!< the rotation angle
	SDL_Point *center;		//!< the rotation point
	SDL_RendererFlip flip;		//!< the flip method
	
	Renderer renderer;
};

/*! \brief render object pointer */
typedef struct render_object *RenderObject;


/*! \brief create a new render object
 *  \return RenderObject or Null on error
 */
RenderObject renderObjectCreate(Renderer renderer);


/*! \brief delete a render object
 *  \param ro the object to destroy
 *  \return true if successful, false if not
 */
bool renderObjectDestroy(RenderObject ro);


/*! \brief create a new render object by combining two textures
 *  	   the position of a will be used for the new object
 *  \param a   the first render object
 *  \param b   the second render object
 *  \return the new render object or NULL on error
 */
RenderObject renderObjectCombine(RenderObject a, RenderObject b);


/*! \brief create a new rectangle texture
 *  \param ro the render object that should store the texture
 *  \param w the width of the rectangle
 *  \param h the height or the rectangle
 *  \return true if successful, false if not
 */
bool renderObjectLoadTextureRect(RenderObject ro, int w, int h);

bool renderObjectLoadTextureCircle(RenderObject ro, double radius);


/*! \brief create a new texture from an image file
 *  \param ro the render object that sould store the texture
 *  \param filename the image file
 *  \return true if successful, false if not
 */
bool renderObjectLoadTextureFromFile(RenderObject ro, const char *filename);


/*! \brief create a new texture from text
 *  \param ro the render object that sould store the texture
 *  \param text the text to render
 *  \param font the font to render with
 *  \return true if successful, false if not
 */
bool renderObjectLoadTextureFromText(RenderObject ro, const char *text, TTF_Font *font);


/*! \brief set the clip rectangle for the texture
 *  \param ro the render object that sould store the texture
 *  \param clip the clip rectangle (default: NULL)
 *  \return true if successful, false if not
 */
bool renderObjectSetClip(RenderObject ro, SDL_Rect *clip);


/*! \brief set the rotation angle of the texture
 *  \param ro the render object that sould store the texture
 *  \param angle the rotation angle in degrees (default: 0.0)
 *  \return true if successful, false if not
 */
bool renderObjectSetAngle(RenderObject ro, double angle);


/*! \brief set the anchor point for rotating the image
 *  \param ro the render object that sould store the texture
 *  \param point the rotation point (default: NULL)
 *  \return true if successful, false if not
 */
bool renderObjectSetCenter(RenderObject ro, SDL_Point point);


/*! \brief set the flip method
 *  \param ro the render object that sould store the texture
 *  \param flip flip method (default: SDL_FLIP_NONE)
 *  \return true if successful, false if not
 */
bool renderObjectSetFlip(RenderObject ro, SDL_RendererFlip flip);


/*! \brief set the texture dimensions and position
 *  \param ro the render object that sould store the texture
 *  \param x the position on the x axis
 *  \param y the position on the y axis
 *  \param w the width
 *  \param h the height
 *  \return true if successful, false if not
 */
bool renderObjectSetDim(RenderObject ro, int x, int y, int w, int h);


/*! \brief set the position of the texture
 *  \param ro the render object that sould store the texture
 *  \param x the position on the x axis
 *  \param y the position on the y axis
 *  \return true if successful, false if not
 */
bool renderObjectSetPos(RenderObject ro, int x, int y);


/*! \brief set the color key for the texture
 * 	   default = cyan (0x00, 0xFF, 0xFF, 0xFF)
 *  \param ro the render object that sould store the texture
 *  \param r the red component
 *  \param g the green component
 *  \param b the blue component
 *  \param a the alpha component
 *  \return true if successful, false if not
 */
bool renderObjectSetColorKey(RenderObject ro, Uint32 r, Uint32 g, Uint32 b, Uint32 a);


/*! \brief set the render color
 * 	   default = black (0x00, 0x00, 0x00, 0xFF)
 *  \param ro the render object that sould store the texture
 *  \param r the red component
 *  \param g the green component
 *  \param b the blue component
 *  \param a the alpha component
 *  \return true if successful, false if not
 */
bool renderObjectSetColor(RenderObject ro, Uint32 r, Uint32 g, Uint32 b, Uint32 a);


void renderObjectSetViewport(RenderObject ro, SDL_Rect *viewport);