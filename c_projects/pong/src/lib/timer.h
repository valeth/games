#pragma once

#include <stdbool.h>


struct timer;

typedef struct timer *Timer;


Timer timerCreate();

bool timerDestroy(Timer timer);


void timerStart(Timer timer);

void timerStop(Timer timer);

void timerPause(Timer timer);

void timerResume(Timer timer);


int timerTicks(Timer timer);


bool timerIsStarted(Timer timer);

bool timerIsPaused(Timer timer);