#pragma once 

#include <stdio.h>
#include <stdbool.h>
#include <errno.h>
#include <string.h>


#define ERROR_FMT "[%-5s] (%s:%-2d %-s)  "
#define clean_errno() (errno == 0 ? "None" : strerror(errno))





#ifdef LOGFILE

#define LOG_START				\
do { 			 			\
	FILE *log_file = fopen(LOGFILE, "w");	\
						\
	if (log_file) {				\
		fclose(log_file);		\
	}					\
} while (0);

#define LOG_TO_FILE(MSG_TYPE, MSG_FMT, ...)				\
do {									\
	FILE *log_file = fopen(LOGFILE, "a");				\
									\
	if (log_file) {							\
		fprintf(log_file, ERROR_FMT  "\n\t" MSG_FMT "\n",	\
			MSG_TYPE, __FILE__, __LINE__,			\
			__func__,  ##__VA_ARGS__);			\
	}								\
									\
	if (log_file) {							\
		fclose(log_file);					\
	}								\
} while (0);
	
#else // ifdef LOGFILE

#define LOG_START()
#define LOG_TO_FILE(MSG_TYPE, MSG_FMT, ...)

#endif
	

#define LOG(MSG_TYPE, MSG_FMT, ...)			\
do {							\
	LOG_TO_FILE(MSG_TYPE, MSG_FMT, ##__VA_ARGS__)	\
							\
	fprintf(stderr, ERROR_FMT "\n\t" MSG_FMT "\n",	\
		MSG_TYPE, __FILE__, __LINE__, __func__,	\
		##__VA_ARGS__);				\
							\
} while (0);


#ifdef NDEBUG
	#define LOG_DEBUG(MSG_FMT, ...)
#else
	#define LOG_DEBUG(MSG_FMT, ...)	LOG("DEBUG", MSG_FMT, ##__VA_ARGS__)
#endif


#define LOG_ERROR(MSG_FMT, ...)				\
	LOG("ERROR", "\b\b\b <errno: %s>)  " MSG_FMT,	\
		clean_errno(), ##__VA_ARGS__)

#define LOG_WARN(MSG_FMT, ...)				\
	LOG("WARN", "\b\b\b <errno: %s>)  " MSG_FMT,	\
		clean_errno(), ##__VA_ARGS__)

#define LOG_INFO(MSG_FMT, ...) 				\
	LOG("INFO", MSG_FMT , ##__VA_ARGS__)
