#include "collision.h"
#include "debug.h"

#include <stdlib.h>
#include <stdio.h>


static double distance(int circle_x, int circle_y, int collision_x, int collision_y);

static bool colCheckCollisionRectangles(ColRect a, ColRect b);

static bool colCheckCollisionTriangles(ColTriangle a, ColTriangle b);

static bool colCheckCollisionCircles(ColCircle a, ColCircle b);

static bool colCheckCollisionRectangleTriangle(ColRect a, ColTriangle b);

static bool colCheckCollisionRectangleCircle(ColRect rect, ColCircle circle);

static bool colCheckCollisionCircleTriangle(ColCircle a, ColTriangle b);


struct collision {
	COLLISION_OBJECT
};

struct col_rect{
	COLLISION_OBJECT
	int width;
	int height;
};

struct col_circle{
	COLLISION_OBJECT
	double radius;
};

struct col_triangle{
	COLLISION_OBJECT
	int hyp;
	int cat;
	double angle;
};


ColRect colCreateRectangle(int x, int y, int width, int height)
{
	ColRect new_rect = malloc(sizeof(*new_rect));

	if (new_rect) {
		new_rect->x = x;
		new_rect->y = y;
		new_rect->type = COL_RECT;

		new_rect->width = width;
		new_rect->height = height;
	} else {
		LOG_ERROR("Failed to create collision rectangle!");
	}

	return new_rect;
}

ColTriangle colCreateTriangle(int x, int y, int hyp, int cat, double angle)
{
	ColTriangle new_triangle = malloc(sizeof(*new_triangle));

	if (new_triangle) {
		new_triangle->x = x;
		new_triangle->y = y;
		new_triangle->type = COL_TRI;

		new_triangle->cat= cat;
		new_triangle->hyp = hyp;
		new_triangle->angle = angle;
	}

	return new_triangle;
}

ColCircle colCreateCircle(int x, int y, double radius)
{
	ColCircle new_circle = malloc(sizeof(*new_circle));

	if (new_circle) {
		new_circle->x = x;
		new_circle->y = y;
		new_circle->type = COL_CIRC;

		new_circle->radius = radius;
	}

	return new_circle;
}

void colObjectDestroy(ColObject col)
{
	if (col) {
		free(col);
		col = NULL;
	}
}


bool colCheckCollision(ColObject a, ColObject b)
{
	bool collided = false;

	switch (a->type | b->type) {
	case COL_RECT:
		collided = colCheckCollisionRectangles((ColRect)a, (ColRect)b);
		break;

	case COL_TRI:
		collided = colCheckCollisionTriangles((ColTriangle)a, (ColTriangle)b);
		break;

	case COL_CIRC:
		collided = colCheckCollisionCircles((ColCircle)a, (ColCircle)b);
		break;

	case COL_RECT | COL_TRI:
		if (a->type == COL_RECT) {
			collided = colCheckCollisionRectangleTriangle((ColRect)a, (ColTriangle)b);
		} else {
			collided = colCheckCollisionRectangleTriangle((ColRect)b, (ColTriangle)a);
		}
		break;

	case COL_RECT | COL_CIRC:
		if (a->type == COL_RECT) {
			collided = colCheckCollisionRectangleCircle((ColRect)a, (ColCircle)b);
		} else {
			collided = colCheckCollisionRectangleCircle((ColRect)b, (ColCircle)a);
		}
		break;

	case COL_CIRC | COL_TRI:
		if (a->type == COL_CIRC) {
			collided = colCheckCollisionCircleTriangle((ColCircle)a, (ColTriangle)b);
		} else {
			collided = colCheckCollisionCircleTriangle((ColCircle)b, (ColTriangle)a);
		}
		break;
	}

	return collided;
}

void colObjectSetPos(ColObject collision, int x, int y)
{
	collision->x = x;
	collision->y = y;
}

static double distance(int circle_x, int circle_y, int collision_x, int collision_y)
{
	int delta_x = collision_x - circle_x;
	int delta_y = collision_y - circle_y;

	return (double)((delta_x * delta_x) + (delta_y * delta_y));
}


static bool colCheckCollisionRectangles(ColRect a, ColRect b)
{
	bool collide = true;

	int a_top = a->y;
	int a_bottom = a->y + a->height;
	int a_left = a->x;
	int a_right = a->x + a->width;

	int b_top = b->y;
	int b_bottom = b->y + b->height;
	int b_left = b->x;
	int b_right = b->x + b->width;

	if (a_top >= b_bottom) {
		collide = false;
	} else if (a_bottom <= b_top) {
		collide = false;
	} else if (a_left >= b_right) {
		collide = false;
	} else if (a_right <= b_left) {
		collide = false;
	}

	return collide;
}

static bool colCheckCollisionTriangles(ColTriangle a, ColTriangle b)
{
	return false;
}

static bool colCheckCollisionCircles(ColCircle a, ColCircle b)
{
	return false;
}

static bool colCheckCollisionRectangleTriangle(ColRect a, ColTriangle b)
{
	return false;
}

static bool colCheckCollisionRectangleCircle(ColRect rect, ColCircle circle)
{
	bool collided = false;

	int collision_x = 0;
	int collision_y = 0;

	if (circle->x < rect->x) {
		collision_x = rect->x;
	} else if (circle->x > (rect->x + rect->width)) {
		collision_x = (rect->x + rect->width);
	} else {
		collision_x = circle->x;
	}

	if (circle->y < rect->y) {
		collision_y = rect->y;
	} else if (circle->y > (rect->y + rect->height)) {
		collision_y = (rect->y + rect->height);
	} else {
		collision_y = circle->y;
	}

	if (distance(circle->x, circle->y, collision_x, collision_y) <= (circle->radius * circle->radius)) {
		collided = true;
	}

	return collided;
}

static bool colCheckCollisionCircleTriangle(ColCircle a, ColTriangle b)
{
	return false;
}
