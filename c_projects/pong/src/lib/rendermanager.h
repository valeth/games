#pragma once

#include "render_object.h"
#include "queue.h"
#include "window.h"

#include <stdbool.h>

#include <SDL.h>



typedef SDL_Renderer *Renderer;

struct rendermanager {
	Renderer renderer;	//!< the hardware renderer
	Queue queue;		//!< the render queue
};

typedef struct rendermanager *RenderManager;



RenderManager rendermanagerCreate(Window window, Uint32 flags);

bool rendermanagerDestroy(RenderManager rem);


bool rendermanagerEnqueue(RenderManager rem, RenderObject robj);

bool rendermanagerRender(RenderManager rem);

void rendermanagerResetRenderer(RenderManager rem);