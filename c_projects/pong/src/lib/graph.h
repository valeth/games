#pragma once

#include "list.h"

#include <stdbool.h>


enum GRAPH_TYPE {
	GRAPH_UNDIRECTED,
	GRAPH_DIRECTED
};

typedef enum GRAPH_TYPE GraphType;

struct vertex;
struct graph;

typedef struct vertex *Vertex;
typedef struct graph *Graph;


Graph graphCreate(int size, GraphType type);

bool graphDestroy(Graph graph);


bool graphAddVertex(Graph graph, int id, void *data);

bool graphAddEdge(Graph graph, int v_id, int w_id);

bool graphIsConnected(Graph graph, int v_id, int w_id);


bool graphSetData(Graph graph, int v_id, void *data);


List graphGetConnectionPath(Graph graph, int v_id, int w_id);

void *graphGetData(Graph graph, int id);

// void *graphVertexGetData(Vertex vertex);

// List graphGetAdjList(Graph graph);