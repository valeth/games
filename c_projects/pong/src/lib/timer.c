#include "timer.h"

#include <stdlib.h>
#include <SDL.h>


struct timer {
	int start_ticks;
	int pause_ticks;

	bool paused;
	bool started;
};


Timer timerCreate()
{
	Timer new_timer = malloc(sizeof(*new_timer));

	if (new_timer) {
		new_timer->start_ticks = 0;
		new_timer->pause_ticks = 0;
		new_timer->paused = false;
		new_timer->started = false;
	} else {
		new_timer = NULL;
	}

	return new_timer;
}

bool timerDestroy(Timer timer)
{
	bool success = true;

	if (timer) {
		free(timer);
	} else {
		success = false;
	}

	return success;
}


void timerStart(Timer timer)
{
	timer->started = true;
	timer->paused = false;

	timer->start_ticks = SDL_GetTicks();
	timer->pause_ticks = 0;
}

void timerStop(Timer timer)
{
	timer->started = false;
	timer->paused = false;

	timer->start_ticks = 0;
	timer->pause_ticks = 0;
}

void timerPause(Timer timer)
{
	if (timer->started && !timer->paused) {
		timer->paused = true;

		timer->pause_ticks = SDL_GetTicks() - timer->start_ticks;
		timer->start_ticks = 0;
	}
}

void timerResume(Timer timer)
{
	if (timer->started && timer->paused) {
		timer->paused = false;

		timer->start_ticks = SDL_GetTicks() - timer->pause_ticks;
		timer->pause_ticks = 0;
	}
}


int timerTicks(Timer timer)
{
	int ticks = 0;

	if (timer->started) {
		if (timer->paused) {
			ticks = timer->pause_ticks;
		} else {
			ticks = SDL_GetTicks() - timer->start_ticks;
		}
	}

	return ticks;
}


bool timerIsStarted(Timer timer)
{
	return timer->started;
}

bool timerIsPaused(Timer timer)
{
	return timer->paused;
}