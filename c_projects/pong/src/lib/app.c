#include "app.h"

#include <stdio.h>
#include <stdlib.h>

#include <SDL_image.h>
#include <SDL_ttf.h>
#include <SDL_mixer.h>


struct app {
	Window window; 			//!< the application window
	RenderManager rem;
	StateHandler sth;

	SDL_Color bg_color;

	SDL_Event event;		//!< the SDL event queue

	bool quit;			//!< quit flag

	const char *title;		//!< the window title
};


App appCreate(const char *title, int w, int h)
{
	bool success = false;
	App new_app = malloc(sizeof(*new_app));


	if (!new_app) {
		success = false;
	} else {
		new_app->title = title;
		success = true;

		// set default background color
		new_app->bg_color.r = 0x00;
		new_app->bg_color.g = 0x00;
		new_app->bg_color.b = 0x00;
		new_app->bg_color.a = 0xFF;
	}


	if (success) {
		if (0 < SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO | SDL_INIT_TIMER)) {
			fprintf(stderr, "SDL could not initialize! SDL Error: %s\n", SDL_GetError());
			success = false;
		}
	}

	if (success) {
		int img_flags = IMG_INIT_PNG;
		if (!(IMG_Init(img_flags) & img_flags)) {
			fprintf(stderr, "SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError());
			success = false;
		}
	}

	if (success) {
		if (0 > Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048)) {
			fprintf(stderr, "SDL_mixer could not initialize! SDL_mixer Error: %s\n", Mix_GetError());
			success = false;
		}
	}

	if (success) {
		if (-1 == TTF_Init()) {
			fprintf(stderr, "SDL_ttf could not initialize! SDL_ttf Error: %s\n", TTF_GetError());
			success = false;
		}
	}


	if (!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1"))
		fprintf(stderr, "Warning: Linear texture filtering not enabled!");

	if (success) {
		new_app->window = windowCreate(new_app->title, -1, -1, w, h, SDL_WINDOW_SHOWN);

		if (!new_app->window)
			success = false;
	}

	// create the render manager
	if (success) {
		new_app->rem = rendermanagerCreate(new_app->window, SDL_RENDERER_ACCELERATED);

		if (!new_app->rem)
			success = false;
	}

	// create state handler
	if (success) {
		new_app->sth = sthCreate();

		if (!new_app->sth)
			success = false;
	}

	// set quit flag if any error occured during initialisation
// 	new_app->quit = !success;

	return new_app;
}

void appDestroy(App app)
{
	if (NULL != app) {
		if (app->rem) {
			rendermanagerDestroy(app->rem);
			app->rem = NULL;
		}

		if (app->sth) {
			sthDestroy(app->sth);
			app->sth = NULL;
		}

		if (app->window) {
			windowDestroy(app->window);
			app->window = NULL;
		}

		free(app);

		TTF_Quit();
		Mix_Quit();
		IMG_Quit();
		SDL_Quit();
	}
}


StateHandler appGetStateHandler(App app)
{
	return app->sth;
}

RenderManager appGetRenderManager(App app)
{
	return app->rem;
}

SDL_Event appGetEventQueue(App app)
{
	return app->event;
}
