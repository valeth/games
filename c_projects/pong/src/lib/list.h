#pragma once

#include <stdbool.h>


struct list_node;
struct list;

typedef struct list_node *ListNode;
typedef struct list *List;


List listCreate();

bool listDestroy(List list);

bool listClear(List list);

bool listClearDestroy(List list);


bool listConcat(List dest, List src);

bool listPush(List list, void *data);

void *listPop(List list);

bool listUnshift(List list, void *data);

void *listShift(List list);


void *listGet(List list, unsigned int pos);

unsigned int listLen(List list);

ListNode listGetHead(List list);

ListNode listGetNext(ListNode node);

void *listGetData(ListNode node);


#define LIST_FOREACH(LIST, CUR_NODE) \
	for (ListNode CUR_NODE = listGetHead(LIST); NULL != CUR_NODE; CUR_NODE = listGetNext(CUR_NODE))
