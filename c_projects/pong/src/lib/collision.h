#pragma once

#include <stdbool.h>

typedef enum {
	COL_RECT = 0x01,
	COL_TRI = 0x02,
	COL_CIRC = 0x04
} ColType;

#define COLLISION_OBJECT 	\
	int x;			\
	int y;			\
	ColType type;

struct collision;
struct col_rect;
struct col_circle;
struct col_triangle;

typedef struct collision *ColObject;
typedef struct col_rect *ColRect;
typedef struct col_circle *ColCircle;
typedef struct col_triangle *ColTriangle;


ColRect colCreateRectangle(int x, int y, int width, int height);

ColTriangle colCreateTriangle(int x, int y, int hyp, int cat, double angle);

ColCircle colCreateCircle(int x, int y, double radius);

void colObjectDestroy(ColObject col);


void colObjectSetPos(ColObject collision, int x, int y);

bool colCheckCollision(ColObject a, ColObject b);

#define CHECK_COLLISION(A, B) colCheckCollision((ColObject)A, (ColObject)B);