#include "queue.h"

#include <stdlib.h>


struct queue {
	unsigned int max;	//!< the maximum number of elements in the queue
	unsigned int start;	//!< the start index
	unsigned int end;	//!< the end index
	bool full;		//!< full flag

	List queue;		//!< the list representing the queue
};


Queue queueCreate(unsigned int size)
{
	Queue new_queue = malloc(sizeof(*new_queue));

	new_queue->max = size;
	new_queue->start = 0;
	new_queue->end = 0;
	new_queue->full = false;
	new_queue->queue = listCreate();

	return new_queue;
}

bool queueDestroy(Queue queue)
{
	listClearDestroy(queue->queue);
	free(queue);
}

bool queueIsEmpty(Queue queue)
{
	if (queue) {
		return (queue->start == queue->end) && !queue->full;
	}
}

bool queueIsFull(Queue queue)
{
	return queue->full;
}

bool queuePut(Queue queue, void* data)
{
	if (!queueIsFull(queue)) {
		listPush(queue->queue, data);
		queue->end = (queue->end + 1) % queue->max;

		if (queue->start == queue->end)
			queue->full = true;
	}
}

void* queueGet(Queue queue)
{
	void *ret = NULL;

	if (!queueIsEmpty(queue)) {
		queue->full = false;
		queue->start = (queue->start + 1) % queue->max;
		ret = listShift(queue->queue);
	}

	return ret;
}

void* queueFront(Queue queue)
{
	if (!queueIsEmpty(queue))
		return listGet(queue->queue, 0);
}
