/******************
 * logging macros *
 ******************/

#pragma once

#include <stdio.h>
#include <errno.h>
#include <string.h>



#define ERROR_FMT "[%-5s]  (%s:%-2d %-s)  "

/* generic print macro */
#define print_log(T, M, ...)				\
	fprintf(stderr, ERROR_FMT M "\n",		\
		T, __FILE__, __LINE__,			\
		__func__, ##__VA_ARGS__)


#define log_err(M, ...)					\
	print_log("ERROR", "\b\b\b <errno: %s>)  " M,	\
		clean_errno(), ##__VA_ARGS__)

#define log_warn(M, ...)				\
	print_log("WARN", "\b\b\b <errno: %s>)  " M,	\
		clean_errno(), ##__VA_ARGS__)

#define log_info(M, ...) 				\
	print_log("INFO", M , ##__VA_ARGS__)

