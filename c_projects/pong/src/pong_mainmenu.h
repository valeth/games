#pragma once

#include "statemanager.h"
#include "rendermanager.h"

#include <SDL.h>


struct pong_mainmenu;

typedef struct pong_mainmenu *MainMenu;


MainMenu pongMainMenuCreate();

void pongMainMenuDestroy(MainMenu menu);


int pongMainMenuEventHandler(StateHandler sth, SDL_Event event);

int pongMainMenuLogicHandler(StateHandler sth);

int pongMainMenuRenderHandler(StateHandler sth, RenderManager rm);