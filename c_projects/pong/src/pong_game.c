#include "pong_game.h"
#include "pong_score.h"
#include "timer.h"
#include "debug.h"

#include <stdlib.h>

#include <SDL_mixer.h>


struct pong_game {
	STATE_OBJECT

	RenderManager rm;
	bool reset;
	bool start;
	bool started;

	// timer for fps independent movement
	Timer global_timer;
	double time_step;

	ScoreBoard scoreboard;

	// the game field
	SDL_Rect game_field;
	RenderObject middle_line;
	RenderObject background;
	PongPlayer player1;
	PongPlayer player2;
	PongBall ball;

	// sounds
	Mix_Chunk *hit_sound;
	Mix_Chunk *start_sound;
};


static int pongCheckWinState(PongGame game);


PongGame pongCreateGame(RenderManager rm, int x, int y, int w, int h)
{
	bool success = true;
	PongGame new_game = malloc(sizeof(*new_game));

	if (new_game) {
		// initialize state object
		new_game->id = 1;
		new_game->render = &pongRenderHandler;
		new_game->logic = &pongLogicHandler;
		new_game->events = &pongEventHandler;

		new_game->rm = rm;
		new_game->reset = false;
		new_game->start = false;
		new_game->started = false;
		new_game->scoreboard = NULL;

		new_game->game_field = (SDL_Rect){
			.x = x,
			.y = h / 10,
			.w = w,
			.h = h - (h / 10)
		};

		new_game->middle_line = NULL;
		new_game->player1 = NULL;
		new_game->player2 = NULL;
		new_game->ball = NULL;
		new_game->hit_sound = NULL;
		new_game->start_sound = NULL;
	} else {
		success = false;
		LOG_ERROR("Failed to create pong game!");
	}

	if (success) {
		ScoreBoard new_sb = pongScoreCreate(rm, x, y, w, h / 10);

		if (new_sb) {
			new_game->scoreboard = new_sb;
		} else {
			success = false;
			new_game->scoreboard = NULL;
			LOG_ERROR("Failed to create scoreboard!");
		}
	}

	if (success) {
		Timer new_timer = timerCreate();

		if (new_timer) {
			new_game->time_step = 0.0;
			new_game->global_timer = new_timer;
		} else {
			success = false;
			new_game->global_timer = NULL;
			LOG_ERROR("Failed to create timer!");
		}
	}

	if (success) {
		RenderObject new_bg = renderObjectCreate(rm->renderer);

		if (new_bg) {
			int width = new_game->game_field.w;
			int height = new_game->game_field.h;

			renderObjectSetColor(new_bg, 0x00, 0x00, 0xFF, 0xFF);
			renderObjectLoadTextureRect(new_bg, width, height);
			renderObjectSetViewport(new_bg, &new_game->game_field);

			new_game->background = new_bg;
		} else {
			success = false;
			new_game->background = NULL;
			LOG_ERROR("Failed to create background!");
		}
	}

	if (success) {
		RenderObject new_middle_line = renderObjectCreate(rm->renderer);

		if (new_middle_line) {
			int x_pos = new_game->game_field.x + (new_game->game_field.w / 2) - 5;
			int y_pos = 0;
			int height = new_game->game_field.h;

			renderObjectSetColor(new_middle_line, 0xFF, 0xFF, 0xFF, 0xFF);
			renderObjectLoadTextureRect(new_middle_line, 10, height);
			renderObjectSetPos(new_middle_line, x_pos, y_pos);
			renderObjectSetViewport(new_middle_line, &new_game->game_field);

			new_game->middle_line = new_middle_line;
		} else {
			success = false;
			new_game->middle_line = NULL;
			LOG_ERROR("Failed to create middle line!");
		}
	}

	if (success) {
		int x_pos = new_game->game_field.x + 10;
		int y_pos = (new_game->game_field.h / 2) - 50;

		PongPlayer new_player = pongPlayerCreate(rm, x_pos, y_pos, 20, 100);

		if (new_player) {
			pongPlayerSetViewport(new_player, &new_game->game_field);
			new_game->player1 = new_player;
		} else {
			success = false;
			new_game->player1 = NULL;
			LOG_ERROR("Failed to create player 1!");

		}
	}

	if (success) {
		int x_pos = new_game->game_field.x + new_game->game_field.w - 30;
		int y_pos = (new_game->game_field.h / 2) - 50;

		PongPlayer new_player = pongPlayerCreate(rm, x_pos, y_pos, 20, 100);

		if (new_player) {
			pongPlayerSetViewport(new_player, &new_game->game_field);
			new_game->player2 = new_player;
		} else {
			success = false;
			new_game->player2 = NULL;
			LOG_ERROR("Failed to create player 2!");
		}
	}

	if (success) {
		int x_pos = new_game->game_field.x + 41;
		int y_pos = (new_game->game_field.h / 2);

		PongBall new_ball = pongBallCreate(rm, x_pos, y_pos, 10);

		if (new_ball) {
			pongBallSetViewport(new_ball, &new_game->game_field);
			new_game->ball = new_ball;
		} else {
			success = false;
			new_game->ball = NULL;
			LOG_ERROR("Failed to create ball!");
		}
	}

	if (success) {
		Mix_Chunk *ball_hit = Mix_LoadWAV("res/pongblipf4.wav");

		if (ball_hit) {
			Mix_VolumeChunk(ball_hit, MIX_MAX_VOLUME / 4);
			new_game->hit_sound = ball_hit;
		} else {
			new_game->hit_sound = NULL;
			LOG_WARN("Failed to load hit sound effect! SDL_mixer Error: %s", Mix_GetError());
		}
	}

	if (success) {
		Mix_Chunk *start_game = Mix_LoadWAV("res/blip-1.wav");

		if (start_game) {
			new_game->start_sound = start_game;
		} else {
			new_game->start_sound = NULL;
			LOG_WARN("Failed to load start sound effect! SDL_mixer Error: %s", Mix_GetError());
		}
	}

	if (!success) {
		pongDestroyGame(new_game);
		new_game = NULL;
	}

	return new_game;
}

void pongDestroyGame(PongGame game)
{
	if (game) {
		if (game->scoreboard) {
			LOG_DEBUG("Freeing scoreboard...");
			pongScoreDestroy(game->scoreboard);
			game->scoreboard = NULL;
		}

		if (game->global_timer) {
			LOG_DEBUG("Freeing timer...");
			timerDestroy(game->global_timer);
			game->global_timer = NULL;
		}

		if (game->middle_line) {
			LOG_DEBUG("Freeing middle line...");
			renderObjectDestroy(game->middle_line);
			game->middle_line = NULL;
		}

		if (game->player1) {
			LOG_DEBUG("Freeing player 1...");
			pongPlayerDestroy(game->player1);
			game->player1 = NULL;
		}

		if (game->player2) {
			LOG_DEBUG("Freeing player 2...");
			pongPlayerDestroy(game->player2);
			game->player2 = NULL;
		}

		if (game->ball) {
			LOG_DEBUG("Freeing ball...");
			pongBallDestroy(game->ball);
			game->ball = NULL;
		}

		if (game->hit_sound) {
			LOG_DEBUG("Freeing hit sound effect...");
			Mix_FreeChunk(game->hit_sound);
			game->hit_sound = NULL;
		}

		if (game->start_sound) {
			LOG_DEBUG("Freeing start sound effect...");
			Mix_FreeChunk(game->start_sound);
			game->start_sound = NULL;
		}

		free(game);
	}
}


static int pongCheckWinState(PongGame game)
{
	int ret = 0;

	if (3 == pongPlayerGetScore(game->player1)) {
		LOG_DEBUG("Player 1 wins!");
		pongPlayerSetScore(game->player1, 0);
		pongPlayerSetScore(game->player2, 0);
		game->reset = true;
		ret = 1;
	} else if (3 == pongPlayerGetScore(game->player2)){
		LOG_DEBUG("Player 2 wins!");
		pongPlayerSetScore(game->player1, 0);
		pongPlayerSetScore(game->player2, 0);
		game->reset = true;
		ret = 2;
	}

	return ret;
}

void pongGameAI(PongPlayer player, PongBall ball, int rand)
{
	switch(pongBallGetDirection(ball)) {
	case PONG_BALL_DIRECTION_UP | PONG_BALL_DIRECTION_LEFT:
		pongPlayerSetDirection(player, PONG_PADDLE_UP);
		break;

	case PONG_BALL_DIRECTION_UP | PONG_BALL_DIRECTION_RIGHT:
		pongPlayerSetDirection(player, PONG_PADDLE_UP);
		break;

	case PONG_BALL_DIRECTION_DOWN | PONG_BALL_DIRECTION_LEFT:
		pongPlayerSetDirection(player, PONG_PADDLE_DOWN);
		break;

	case PONG_BALL_DIRECTION_DOWN | PONG_BALL_DIRECTION_RIGHT:
		pongPlayerSetDirection(player, PONG_PADDLE_DOWN);
		break;

	default:
		pongPlayerSetDirection(player, PONG_PADDLE_STOP);
		break;
	}
}


// the state handlers
int pongEventHandler(StateHandler sth, SDL_Event event)
{
	PongGame pong = CURRENT_STATE(sth, PongGame);

	while (SDL_PollEvent(&event)) {
		switch (event.type) {
		case SDL_QUIT:
			sthChangeState(sth, 0);
			break;

		case SDL_KEYDOWN:
			if (event.key.repeat == 0) {
				switch (event.key.keysym.sym) {
				case SDLK_w:
					pongPlayerSetDirection(pong->player1, PONG_PADDLE_UP);
					break;

				case SDLK_s:
					pongPlayerSetDirection(pong->player1, PONG_PADDLE_DOWN);
					break;

				case SDLK_i:
					pongPlayerSetDirection(pong->player2, PONG_PADDLE_UP);
					break;

				case SDLK_k:
					pongPlayerSetDirection(pong->player2, PONG_PADDLE_DOWN);
					break;

				case SDLK_SPACE:
					if (!pong->started) {
						pong->started = true;
						pong->start = true;
					}
					break;

				case SDLK_r:
					pong->reset = true;
					break;

				case SDLK_ESCAPE:
					sthChangeState(sth, 0);
					break;
#ifndef NDEBUG
				case SDLK_PLUS:
					pongPlayerIncreaseScore(pong->player1);
					break;

				case SDLK_MINUS:
					pongPlayerDecreaseScore(pong->player1);
					break;
#endif
				}
			}

			break;

		case SDL_KEYUP:
			if (event.key.repeat == 0) {
				switch (event.key.keysym.sym) {
				case SDLK_w:
				case SDLK_s:
					pongPlayerSetDirection(pong->player1, PONG_PADDLE_STOP);
					break;

				case SDLK_i:
				case SDLK_k:
					pongPlayerSetDirection(pong->player2, PONG_PADDLE_STOP);
					break;
				}
			}

			break;
		}
	}

	return 0;
}

int pongRenderHandler(StateHandler sth, RenderManager rm)
{
	PongGame pong = CURRENT_STATE(sth, PongGame);

	pongScoreRender(pong->scoreboard);
	rendermanagerEnqueue(rm, pong->background);
	rendermanagerEnqueue(rm, pong->middle_line);

	pongPlayerRender(pong->player1);
	pongPlayerRender(pong->player2);
	pongBallRender(pong->ball);

	rendermanagerRender(rm);

	return 0;
}

int pongLogicHandler(StateHandler sth)
{
	PongGame pong = CURRENT_STATE(sth, PongGame);

	if (pongBallInPlayer1Goal(pong->ball)) {
		pongPlayerIncreaseScore(pong->player2);
		pong->reset = true;
	} else if (pongBallInPlayer2Goal(pong->ball)) {
		pongPlayerIncreaseScore(pong->player1);
		pong->reset = true;
	}

	pongCheckWinState(pong);

	if (pong->start) {
		pong->start = false;
		pongBallSetVelocity(pong->ball, 700.0, 700.0);
		pongBallSetState(pong->ball, PONG_BALL_GO);
	}

	if (pong->reset) {
		pong->reset = false;
		pong->started = false;
		pong->start = false;
		pongBallSetVelocity(pong->ball, 0.0, 0.0);
		pongBallSetState(pong->ball, PONG_BALL_STOP);
		pongBallSetPos(pong->ball, (pong->game_field.x + 41), (pong->game_field.h / 2));
		pongPlayerSetPos(pong->player1, (pong->game_field.x + 10), (pong->game_field.h / 2) - 50);
		pongPlayerSetPos(pong->player2, (pong->game_field.x + pong->game_field.w) - 30, (pong->game_field.h / 2) - 50);
		Mix_PlayChannel(-1, pong->start_sound, 0);
	}

	pong->time_step = timerTicks(pong->global_timer) / 1000.0;

	pongScoreUpdate(pong->scoreboard, pong->player1, pong->player2);

	// check collision
	if (pongBallCheckCollision(pong->ball, pong->player1, pong->player2, pong->game_field)) {
		Mix_PlayChannel(-1, pong->hit_sound, 0);
	}

	pongPlayerCheckCollision(pong->player1, pong->game_field);
	pongPlayerCheckCollision(pong->player2, pong->game_field);

	// move stuff
// 	pongGameAI(pong->player2, pong->ball, 10);
// 	pongGameAI(pong->player1, pong->ball, 10);
	pongBallMove(pong->ball, pong->time_step);
	pongPlayerMove(pong->player1, pong->time_step);
	pongPlayerMove(pong->player2, pong->time_step);

	timerStart(pong->global_timer);

	return 0;
}