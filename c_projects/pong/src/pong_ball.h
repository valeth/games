#pragma once

#include "pong_player.h"
#include "collision.h"

#include <stdbool.h>

#include <SDL.h>


typedef enum {
	PONG_BALL_STOP = 0,
	PONG_BALL_GO
} PongBallState;

typedef enum {
	PONG_BALL_DIRECTION_STOP = 0x00,
	PONG_BALL_DIRECTION_UP = 0x01,
	PONG_BALL_DIRECTION_DOWN = 0x02,
	PONG_BALL_DIRECTION_LEFT = 0x04,
	PONG_BALL_DIRECTION_RIGHT = 0x08
} PongBallDirection;

struct pong_ball;

typedef struct pong_ball *PongBall;


PongBall pongBallCreate(RenderManager rm, int x, int y, double r);

void pongBallDestroy(PongBall ball);


bool pongBallCheckCollision(PongBall ball, PongPlayer player1, PongPlayer player2, SDL_Rect border);

void pongBallMove(PongBall ball, double step);

void pongBallRender(PongBall ball);


PongBallDirection pongBallGetDirection(PongBall ball);

bool pongBallInPlayer1Goal(PongBall ball);

bool pongBallInPlayer2Goal(PongBall ball);

void pongBallSetViewport(PongBall ball, SDL_Rect *viewport);

void pongBallSetPos(PongBall ball, double x, double y);

void pongBallSetState(PongBall ball, PongBallState state);

void pongBallSetVelocity(PongBall ball, double x_vel, double y_vel);