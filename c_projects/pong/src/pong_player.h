#pragma once

#include "pong_paddle.h"
#include "rendermanager.h"

#include <stdbool.h>

#include <SDL.h>


struct pong_player;

typedef struct pong_player *PongPlayer;


PongPlayer pongPlayerCreate(RenderManager rm, int x, int y, int w, int h);

void pongPlayerDestroy(PongPlayer player);


void pongPlayerRender(PongPlayer player);

bool pongPlayerCheckCollision(PongPlayer player, SDL_Rect border);

void pongPlayerMove(PongPlayer player, double step);

ColObject pongPlayerGetCollider(PongPlayer player);

void pongPlayerIncreaseScore(PongPlayer player);

void pongPlayerDecreaseScore(PongPlayer player);


void pongPlayerSetScore(PongPlayer player, int score);

void pongPlayerSetViewport(PongPlayer player, SDL_Rect *viewport);

void pongPlayerSetDirection(PongPlayer player, PongPaddleDirection direction);

void pongPlayerSetPos(PongPlayer player, int x, int y);

unsigned int pongPlayerGetScore(PongPlayer player);