#pragma once

#include "render_object.h"
#include "collision.h"
#include "rendermanager.h"

#include <stdbool.h>

#include <SDL.h>


typedef enum {
	PONG_PADDLE_UP = -1,
	PONG_PADDLE_STOP,
	PONG_PADDLE_DOWN
} PongPaddleDirection;


struct pong_paddle;

typedef struct pong_paddle *PongPaddle;


PongPaddle pongPaddleCreate(RenderManager rm, int x, int y, int w, int h);

void pongPaddleDestroy(PongPaddle paddle);


bool pongPaddleCollideWithBorder(PongPaddle paddle, SDL_Rect border);

void pongPaddleMove(PongPaddle paddle, double step);

void pongPaddleRender(PongPaddle paddle);

ColObject pongPaddleGetCollider(PongPaddle paddle);


void pongPaddleSetPos(PongPaddle paddle, double x, double y);

void pongPaddleSetViewport(PongPaddle paddle, SDL_Rect *viewport);

void pongPaddleSetDirection(PongPaddle paddle, PongPaddleDirection direction);