#include "pong_game.h"
#include "app.h"
#include "debug.h"


int main(int argc, char **argv)
{
	const int screen_width = 640;
	const int screen_height = 480;

	LOG_START;

	int ret = 0;
	App app = appCreate("Pong!", screen_width, screen_height);

	if (!app) {
		LOG_ERROR("Failed to initialize!\n");
		ret = 1;
	}

	if (0 == ret) {
		StateHandler sth = appGetStateHandler(app);

		PongGame pong = pongCreateGame(appGetRenderManager(app), 0, 0, screen_width, screen_height);

		if (!pong) {
			sthChangeState(sth, 0);
			sthSetState(sth);
			LOG_ERROR("Failed to create pong game!");
		} else {
			sthAddState(sth, 1, (State)pong);
			sthChangeState(sth, 1);
			sthSetState(sth);
		}

		while (0 != sthGetCurrentStateId(sth)) {

			sthCurrentEvents(sth, appGetEventQueue(app));
			sthCurrentLogic(sth);
			sthCurrentRender(sth, appGetRenderManager(app));

			sthSetState(sth);
		}

		pongDestroyGame(pong);
	}

	appDestroy(app);

	return ret;
}
