#pragma once

#include "pong_player.h"
#include "rendermanager.h"

#include <SDL.h>
#include <SDL_ttf.h>


struct score_board;

typedef struct score_board *ScoreBoard;


ScoreBoard pongScoreCreate(RenderManager rm, int x, int y, int w, int  h);

void pongScoreDestroy(ScoreBoard score);


void pongScoreRender(ScoreBoard score);

void pongScoreUpdate(ScoreBoard score, PongPlayer player1, PongPlayer player2);


SDL_Rect pongScoreGetBoard(ScoreBoard score);