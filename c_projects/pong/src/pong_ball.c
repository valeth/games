#include "pong_ball.h"
#include "render_object.h"
#include "debug.h"

#include <stdlib.h>

#include <SDL.h>


static bool pongBallCollideWithPlayer(PongBall ball, PongPlayer player);

static bool pongBallCollideWithGoal(PongBall ball, SDL_Rect border);

static bool pongBallCollideWithBorder(PongBall ball, SDL_Rect border);


struct pong_ball {
	double x;
	double y;
	double radius;
	double x_vel;
	double y_vel;

	RenderManager rm;
	RenderObject circle;

	ColCircle collision;
	bool top_collided;
	bool bot_collided;
	bool player1_collided;
	bool player2_collided;
	bool in_player1_goal;
	bool in_player2_goal;

	PongBallState state;
};


PongBall pongBallCreate(RenderManager rm, int x, int y, double r)
{
	bool success = true;
	PongBall new_ball = malloc(sizeof(*new_ball));

	if (new_ball) {
		new_ball->x = x;
		new_ball->y = y;
		new_ball->x_vel = 0.0;
		new_ball->y_vel = 0.0;
		new_ball->radius = r;
		new_ball->state = PONG_BALL_STOP;
		new_ball->top_collided = false;
		new_ball->bot_collided = false;
		new_ball->player1_collided = false;
		new_ball->player2_collided = false;
		new_ball->in_player1_goal = false;
		new_ball->in_player2_goal = false;
		new_ball->rm = rm;
	} else {
		success = false;
	}

	if (success) {
		RenderObject new_circle = renderObjectCreate(rm->renderer);

		if (new_circle) {
			renderObjectSetColor(new_circle, 0xFF, 0xFF, 0xFF, 0xFF);
			renderObjectLoadTextureCircle(new_circle, r);
			renderObjectSetPos(new_circle, x, y);
		} else {
			success = false;
		}

		new_ball->circle = new_circle;
	}

	if (success) {
		ColCircle new_collision = colCreateCircle(x, y, r);

		if (!new_collision) {
			success = false;
		}

		new_ball->collision = new_collision;
	}

	if (!success) {
		pongBallDestroy(new_ball);
		new_ball = NULL;
	}

	return new_ball;
}

void pongBallDestroy(PongBall ball)
{
	if (ball) {
		if (ball->circle) {
			renderObjectDestroy(ball->circle);
			ball->circle = NULL;
		}

		if (ball->collision) {
			colObjectDestroy((ColObject)ball->collision);
			ball->collision = NULL;
		}

		free(ball);
	}
}


static bool pongBallCollideWithPlayer(PongBall ball, PongPlayer player)
{
	return CHECK_COLLISION(ball->collision, pongPlayerGetCollider(player));
}

static bool pongBallCollideWithGoal(PongBall ball, SDL_Rect border)
{
	ColRect player1_goal = colCreateRectangle(border.x, 0, 0, border.h);
	ColRect player2_goal = colCreateRectangle(border.x + border.w, 0, 0, border.h);

	ball->in_player1_goal = CHECK_COLLISION(ball->collision, player1_goal);
	ball->in_player2_goal = CHECK_COLLISION(ball->collision, player2_goal);

	colObjectDestroy((ColObject)player1_goal);
	colObjectDestroy((ColObject)player2_goal);

	return ball->in_player1_goal || ball->in_player2_goal;
}

static bool pongBallCollideWithBorder(PongBall ball, SDL_Rect border)
{
	ColRect top_border = colCreateRectangle(border.x, 0, border.w, 0);
	ColRect bot_border = colCreateRectangle(border.x, border.h, border.w, 0);

	ball->top_collided = CHECK_COLLISION(ball->collision, top_border);
	ball->bot_collided = CHECK_COLLISION(ball->collision, bot_border);

	colObjectDestroy((ColObject)top_border);
	colObjectDestroy((ColObject)bot_border);

	return ball->top_collided || ball->bot_collided;
}

bool pongBallCheckCollision(PongBall ball, PongPlayer player1, PongPlayer player2, SDL_Rect border)
{
	ball->player1_collided = pongBallCollideWithPlayer(ball, player1);
	ball->player2_collided = pongBallCollideWithPlayer(ball, player2);
	pongBallCollideWithGoal(ball, border);
	pongBallCollideWithBorder(ball, border);

	return (ball->player1_collided || ball->player2_collided || ball->top_collided || ball->bot_collided);
}

void pongBallMove(PongBall ball, double step)
{
	double x_next = ball->x;
	double y_next = ball->y;

	switch (ball->state) {
	case PONG_BALL_GO:
		if (ball->top_collided) {
			y_next += ball->radius * 2;
			ball->y_vel = -ball->y_vel;
		} else if (ball->bot_collided) {
			y_next -= ball->radius * 2;
			ball->y_vel = -ball->y_vel;
		} else if (ball->player1_collided) {
			x_next += ball->radius * 2;
			ball->x_vel = -ball->x_vel;
		} else if (ball->player2_collided) {
			x_next -= ball->radius * 2;
			ball->x_vel = -ball->x_vel;

		} else {
			x_next += ball->x_vel * step;
			y_next += ball->y_vel * step;
		}

		break;

	case PONG_BALL_STOP:
		ball->x_vel = abs(ball->x_vel);
		ball->y_vel = abs(ball->y_vel);
		break;
	}

	pongBallSetPos(ball, x_next, y_next);
}

void pongBallRender(PongBall ball)
{
	rendermanagerEnqueue(ball->rm, ball->circle);
}

bool pongBallInPlayer1Goal(PongBall ball)
{
	return ball->in_player1_goal;
}

bool pongBallInPlayer2Goal(PongBall ball)
{
	return ball->in_player2_goal;
}

void pongBallSetViewport(PongBall ball, SDL_Rect *viewport)
{
	renderObjectSetViewport(ball->circle, viewport);
}

void pongBallSetPos(PongBall ball, double x, double y)
{
	ball->x = x;
	ball->y = y;

	renderObjectSetPos(ball->circle, (int)(x - ball->radius), (int)(y - ball->radius));
	colObjectSetPos((ColObject)ball->collision, x, y);
}

void pongBallSetState(PongBall ball, PongBallState state)
{
	ball->state = state;
}

PongBallDirection pongBallGetDirection(PongBall ball)
{
	PongBallDirection direction = PONG_BALL_DIRECTION_STOP;

	if (ball->x_vel > 0) {
		direction |= PONG_BALL_DIRECTION_RIGHT;
	} else if (ball->x_vel < 0) {
		direction |= PONG_BALL_DIRECTION_LEFT;
	}

	if (ball->y_vel > 0) {
		direction |= PONG_BALL_DIRECTION_DOWN;
	} else if (ball->y_vel < 0) {
		direction |= PONG_BALL_DIRECTION_UP;
	}

	return direction;
}

void pongBallSetVelocity(PongBall ball, double x_vel, double y_vel)
{
	ball->x_vel = x_vel;
	ball->y_vel = y_vel;
}