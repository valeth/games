#include "pong_player.h"
#include "debug.h"

#include <stdbool.h>


struct pong_player {
	RenderManager rm;
	PongPaddle paddle;
	int score;
};


// construction/destruction functions
PongPlayer pongPlayerCreate(RenderManager rm, int x, int y, int w, int h)
{
	bool success = true;
	PongPlayer new_player = malloc(sizeof(*new_player));

	if (new_player) {
		new_player->rm = rm;
		new_player->score = 0;
	} else {
		success = false;
	}

	if (success) {
		PongPaddle new_paddle = pongPaddleCreate(rm, x, y, w, h);

		if (!new_paddle) {
			success = false;
			LOG_ERROR("Failed to create paddle!");
		}

		new_player->paddle = new_paddle;
	}

	if (!success) {
		pongPlayerDestroy(new_player);
		new_player = NULL;
		LOG_ERROR("Failed to create player!");
	}

	return new_player;
}

void pongPlayerDestroy(PongPlayer player)
{
	if (player) {
		if (player->paddle) {
			pongPaddleDestroy(player->paddle);
			player->paddle = NULL;
		}

		free(player);
	}
}


void pongPlayerRender(PongPlayer player)
{
	pongPaddleRender(player->paddle);
}

void pongPlayerIncreaseScore(PongPlayer player)
{
	pongPlayerSetScore(player, player->score + 1);
}

void pongPlayerDecreaseScore(PongPlayer player)
{
	pongPlayerSetScore(player, player->score - 1);
}


// mutator functions
void pongPlayerSetScore(PongPlayer player, int score)
{
	if (score >= 0) {
		LOG_DEBUG("old score: %d, new score %d", player->score, score);
		player->score = score;
	}
}

void pongPlayerSetViewport(PongPlayer player, SDL_Rect *viewport)
{
	pongPaddleSetViewport(player->paddle, viewport);
}

void pongPlayerSetDirection(PongPlayer player, PongPaddleDirection direction)
{
	pongPaddleSetDirection(player->paddle, direction);
}

void pongPlayerSetPos(PongPlayer player, int x, int y)
{
	pongPaddleSetPos(player->paddle, x, y);
}


// accessor functions
bool pongPlayerCheckCollision(PongPlayer player, SDL_Rect border)
{
	return pongPaddleCollideWithBorder(player->paddle, border);
}

void pongPlayerMove(PongPlayer player, double step)
{
	pongPaddleMove(player->paddle, step);
}

ColObject pongPlayerGetCollider(PongPlayer player)
{
	return (ColObject)pongPaddleGetCollider(player->paddle);
}

unsigned int pongPlayerGetScore(PongPlayer player)
{
	return player->score;
}