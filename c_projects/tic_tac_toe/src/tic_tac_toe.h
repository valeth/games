/*!
 * \file tic_tac_toe.h
 * \author Patrick Auernig
 * \date 08.09.2015
 * \brief tic tac toe game
 * 
 * \see https://github.com/valeth/tic_tac_toe
 */

#pragma once

#include "rendermanager.h"
#include "statemachine.h"

#include <stdbool.h>

#include <SDL_mixer.h>
#include <SDL_ttf.h>


struct ttt_field {
	RenderObject ro;	//!< the base render object
	RenderObject overlay;	//!< the overlay render object (X or O)
	
	int owner;		//!< the current owner of the field
};

typedef struct ttt_field *TTTField;


struct ttt_board {
	// state
	STATE_OBJECT
	
	// the board
	TTTField **board;		//!< the board fields
	RenderManager rem;
	SDL_Rect dim;			//!< the board dimensions
	
	// resources
	RenderObject cross;		//!< render object containing the cross texture
	RenderObject circle;		//!< render object containing the circle texture
	Mix_Chunk *button_click;	//!< the click sound effect
	
	int turn;			//!< the current turn
	int winner;			//!< the winner
	bool reset;
	bool clicked;
};

typedef struct ttt_board *TTTBoard;


struct ttt_message {
	// state
	STATE_OBJECT
	
	// the message
	RenderObject text;
	// reference to board
	TTTBoard board;
};


typedef struct ttt_message *TTTMessage;
	


TTTMessage tttMessageCreate(RenderManager rem, char *text, TTF_Font* font, int x, int y, int width, int height);


bool tttMessageDestroy(TTTMessage msg);


static TTTField tttFieldCreate(Renderer renderer);


static bool tttFieldDestroy(TTTField field);


static TTTBoard tttBoardCreate(RenderManager rem, int width);


static bool tttBoardDestroy(TTTBoard board);



static bool tttFieldSetOwner(TTTField field, int owner);


static int tttRowEqual(TTTField field[], size_t size);


static int tttColEqual(TTTField **field, int j, size_t size);


static bool tttFieldSetOverlay(TTTBoard board, TTTField field);


static void tttPrintOwnerMap(TTTBoard board);


static bool tttBoardReset(TTTBoard board);



int tttEventHandler(StateHandler sth, SDL_Event event);


int tttRenderHandler(StateHandler sth, RenderManager rem);


int tttLogicHandler(StateHandler sth);


int tttMsgEventHandler(StateHandler sth, SDL_Event event);


int tttMsgRenderHandler(StateHandler sth, RenderManager rem);