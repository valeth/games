#include "tic_tac_toe.h"
#include "app.h"

#include <stdlib.h>
#include <stdio.h>



int main(int argc, char **argv)
{
	App app = appCreate("Tic Tac Toe", 299, 299);
	StateHandler sth = app->sth;
	
	int success = 0;
	TTF_Font *font = TTF_OpenFont("res/liberation_mono_regular.ttf", 14);
	
	if (!font) {
		printf("Failed to open font!\n");
	}
	
	// the game board
	TTTBoard board = tttBoardCreate(app->rem, 3);
	sthAddState(sth, 1, (State)board);
	
	TTTMessage win1_msg = tttMessageCreate(app->rem, "Player 1 wins!", font, 0, 30, 300, 14);
	win1_msg->render = &tttMsgRenderHandler;
	win1_msg->events = &tttMsgEventHandler;
	win1_msg->logic = NULL;
	win1_msg->id = 2;
	win1_msg->board = board;
	sthAddState(sth, 2, (State)win1_msg);
	
	TTTMessage win2_msg = tttMessageCreate(app->rem, "Player 2 wins!", font, 0, 30, 300, 14);
	win2_msg->render = &tttMsgRenderHandler;
	win2_msg->events = &tttMsgEventHandler;
	win2_msg->logic = NULL;
	win2_msg->id = 3;
	win2_msg->board = board;
	sthAddState(sth, 3, (State)win2_msg);
	
	TTTMessage end1_msg = tttMessageCreate(app->rem, "No more moves left!", font, 0, 30, 300, 14);
	end1_msg->render = &tttMsgRenderHandler;
	end1_msg->events = &tttMsgEventHandler;
	end1_msg->logic = NULL;
	end1_msg->id = 4;
	end1_msg->board = board;
	sthAddState(sth, 4, (State)end1_msg);
	
	// change to board state
	sthChangeState(sth, 1);
	sthSetState(sth);
	
	while (0 != sthGetState(sth)) {
		sthCurrentEvents(sth, app->event);
		
		sthCurrentLogic(sth);

		sthSetState(sth);
		
		sthCurrentRender(sth, app->rem);
		
		rendermanagerRender(app->rem);
	}
	
	// cleanup
	tttMessageDestroy(win1_msg);
	tttMessageDestroy(win2_msg);
	tttMessageDestroy(end1_msg);
	tttBoardDestroy(board);
	TTF_CloseFont(font);
	appDestroy(app);

	return success;
}



TTTMessage tttMessageCreate(RenderManager rem, char *text, TTF_Font *font, int x, int y, int width, int height)
{
	bool success = true;
	TTTMessage new_message = malloc(sizeof(*new_message));
	RenderObject message_rect = NULL;
	RenderObject tmp_text = NULL;
	
	if (!new_message) {
		success = false;
	}
	
	if (success) {
		message_rect = renderObjectCreate(rem->renderer);
		
		if (!message_rect) {
			success = false;
		} else {
			renderObjectSetColor(message_rect, 0xBB, 0xCC, 0xDD, 0xFF);
			renderObjectLoadTextureRect(message_rect, width, height);
		}
	}
	
	if (success) {
		tmp_text = renderObjectCreate(rem->renderer);
		
		if (!tmp_text) {
			success = false;
		} else {
			renderObjectSetColor(tmp_text, 0xFF, 0x1C, 0x00, 0xFF);
			renderObjectLoadTextureFromText(tmp_text, text, font);
		}
	}

	if (success) {
		new_message->text = renderObjectCombine(message_rect, tmp_text);
		renderObjectSetPos(new_message->text, x, y);
	}
		
	return new_message;
}


bool tttMessageDestroy(TTTMessage msg)
{
	bool success = true;
	
	if (msg) {
		if (msg->text) {
			renderObjectDestroy(msg->text);
		}
		
		msg->board = NULL;
		
		free(msg);
	} else {
		success = false;
	}
	
	return success;
}



// tic tac toe board handlers
int tttEventHandler(StateHandler sth, SDL_Event event)
{
	TTTBoard board = (TTTBoard)sth->current;
	
	while (SDL_PollEvent(&event)) {
		switch(event.type) {
		case SDL_QUIT:
			sthChangeState(sth, 0);
			break;
			
		case SDL_KEYDOWN:
			switch(event.key.keysym.sym) {
			case SDLK_r:
				board->reset = true;
				sthChangeState(sth, 1);
				break;
				
			case SDLK_ESCAPE:
				sthChangeState(sth, 0);
				break;
			}
			
			break;

		case SDL_MOUSEBUTTONDOWN:
			board->clicked = true;
			break;
		}
	}
}

int tttLogicHandler(StateHandler sth)
{
	TTTBoard board = (TTTBoard)sth->current;
	int winner = -1;
	
	if (board->reset) {
		tttBoardReset(board);
	}
	
	if (0 == board->winner) {
		board->reset = true;
		sthChangeState(sth, 2);
	} else if (1 == board->winner) {
		board->reset = true;
		sthChangeState(sth, 3);
	} else if (9 == board->turn) {
		board->reset = true;
		sthChangeState(sth, 4);
	}
	
	for (int i = 0; i < board->dim.w; ++i) {
		board->winner = tttRowEqual(board->board[i], 3);
		
		if (-1 != board->winner) {
			break;
		}
	}
	
	
	if (-1 == board->winner) {
		for (int j = 0; j < board->dim.w; ++j) {
			board->winner = tttColEqual(board->board, j, 3);
			
			if (-1 != board->winner) {
				break;
			}
		}
	}
	
	if (-1 == board->winner) {
		int *winner_check = malloc(sizeof(*winner_check) * board->dim.w);
		
		for (int i = 0, j = 0; i < board->dim.w; ++i) {
			for (; j < i; ++j);
			
			winner_check[i] = board->board[i][j]->owner;
		}
		
		if ((-1 != winner_check[0]) && (winner_check[0] == winner_check[1]) && (winner_check[0] == winner_check[2])) {
			board->winner = winner_check[0];
		}
		
		free(winner_check);
	}
	
	if (-1 == board->winner) {
		int *winner_check = malloc(sizeof(*winner_check) * board->dim.w);
		
		for (int i = 2, j = 0; j < board->dim.w; --i, ++j) {
			winner_check[i] = board->board[i][j]->owner;
		}
		
		if ((-1 != winner_check[0]) && (winner_check[0] == winner_check[1]) && (winner_check[0] == winner_check[2])) {
			board->winner = winner_check[0];
		}
		
		free(winner_check);
	}
	
	if (-1 == board->winner && board->clicked) {
		int x;
		int y;
		
		SDL_GetMouseState(&x, &y);
		
		for (int i = 0; i < board->dim.w; ++i) {
			for (int j = 0; j < 3; ++j) {
				TTTField f = board->board[i][j];
				
				if (-1 != f->owner)
					continue;
				
				if (x < f->ro->dim.x)
					continue;
				else if (x > f->ro->dim.x + f->ro->dim.w)
					continue;
				else if (y < f->ro->dim.y)
					continue;
				else if (y > f->ro->dim.y + f->ro->dim.h)
					continue;
				else {
					tttFieldSetOwner(f, board->turn % 2);
					Mix_PlayChannel(-1, board->button_click, 0);
					++board->turn;
				}
			}
		}
		
		board->clicked = false;
	}
}

int tttRenderHandler(StateHandler sth, RenderManager rem)
{
	
	TTTBoard board = (TTTBoard)sth->current;
	
	for (int i = 0; i < board->dim.w; ++i) {
		for (int j = 0; j < board->dim.w; ++j) {
			rendermanagerEnqueue(rem, board->board[i][j]->ro);
			
			tttFieldSetOverlay(board, board->board[i][j]);
			
			if (NULL != board->board[i][j]->overlay)
				rendermanagerEnqueue(rem, board->board[i][j]->overlay);
		}
	}
}


int tttMsgEventHandler(StateHandler sth, SDL_Event event)
{
	TTTMessage msg = (TTTMessage)sth->current;
	
	while (SDL_PollEvent(&event)) {
		switch(event.type) {
		case SDL_QUIT:
			sthChangeState(sth, 0);
			break;
			
		case SDL_KEYDOWN:
			switch(event.key.keysym.sym) {
			case SDLK_r:
				msg->board->reset = true;
				sthChangeState(sth, 1);
			}
		}
	}
}

int tttMsgRenderHandler(StateHandler sth, RenderManager rem)
{
	TTTMessage tmp_msg = (TTTMessage)sth->current;
	TTTBoard board = tmp_msg->board;
	
	for (int i = 0; i < board->dim.w; ++i) {
		for (int j = 0; j < board->dim.w; ++j) {
			rendermanagerEnqueue(rem, board->board[i][j]->ro);
			
			tttFieldSetOverlay(board, board->board[i][j]);
			
			if (NULL != board->board[i][j]->overlay)
				rendermanagerEnqueue(rem, board->board[i][j]->overlay);
		}
	}
			
	rendermanagerEnqueue(rem, tmp_msg->text);
}



static TTTField tttFieldCreate(Renderer renderer)
{
	TTTField new_field = malloc(sizeof(*new_field));
	
	if (new_field) {
		// the texture
		new_field->ro = renderObjectCreate(renderer);
		new_field->overlay = NULL;
		
		// set owner:
		//	-1 = none
		//	0 = player 1
		//	1 = player 2
		new_field->owner = -1;
	
	} else {
		fprintf(stderr, "Failed to create new field!\n");
	}
	
	return new_field;
}


static bool tttFieldDestroy(TTTField field)
{
	bool ret = true;
	
	if (field) {
		if (field->ro) {
			renderObjectDestroy(field->ro);
			field->ro = NULL;
		}
		
		if (field->overlay) {
			renderObjectDestroy(field->overlay);
			field->overlay = NULL;
		}
		
		free(field);
	} else {
		ret = false;
	}
	
	return ret;
}


static TTTBoard tttBoardCreate(RenderManager rem, int width)
{
	bool success = true;
	TTTBoard new_board = NULL;
	
	if (!rem)
		success = false;
	
	if (success) {
		new_board = malloc(sizeof(*new_board));
		
		if (!new_board) {
			fprintf(stderr, "Failed to create new game board!\n");
			success = false;
		} else {
			new_board->id = 1;
			new_board->events = &tttEventHandler;
			new_board->logic = &tttLogicHandler;
			new_board->render = &tttRenderHandler;
			
			new_board->dim.x = 0;
			new_board->dim.y = 0;
			new_board->dim.w = width;
			new_board->dim.h = width;
			new_board->turn = 0;
			new_board->winner = -1;
			new_board->rem = rem;
			new_board->reset = false;
			new_board->clicked = false;
		}
	}
	
	if (success) {
		new_board->circle = renderObjectCreate(new_board->rem->renderer);
		
		if (!new_board->circle) {
			fprintf(stderr, "Failed to load circle render object!\n");
			success = false;
		} else {
			renderObjectLoadTextureFromFile(new_board->circle, "res/circle.png");
		}
	}
		
	if (success) {
		new_board->cross = renderObjectCreate(new_board->rem->renderer);
		
		if (!new_board->cross) {
			fprintf(stderr, "Failed to load cross render object!\n");
			success = false;
		} else {
			renderObjectLoadTextureFromFile(new_board->cross, "res/cross.png");
		}
	}
			
	if (success) {
		new_board->button_click = Mix_LoadWAV("res/button_click.wav");
		
		if (!new_board->button_click) {
			fprintf(stderr, "Failed to load click sound!\n");
			success = false;
		}
	}
	
	if (success) {
		new_board->board = malloc(sizeof(*new_board->board) * new_board->dim.w);
	
		if (!new_board->board) {
			fprintf(stderr, "Failed to create new board!\n");
			success = false;
		}
	}
	
	if (success) {
		for (int i = 0; i < new_board->dim.w; ++i) {
			new_board->board[i] = malloc(sizeof(*new_board->board[i]) * new_board->dim.w);
		
			if (!new_board->board[i]) {
				fprintf(stderr, "Failed to allocate board array!\n");
				success = false;
			}
		}
	}
	
	if (success) {
		int rect_offset = 100;
		
		for (int i = 0; success && i < new_board->dim.w; ++i) {
			for (int j = 0; j < new_board->dim.w; ++j) {
				new_board->board[i][j] = tttFieldCreate(new_board->rem->renderer);
			
				if (!new_board->board[i][j]) {
					fprintf(stderr, "Failed to create board field!\n");
					success = false;
					break;
				}
			
				renderObjectSetColor(new_board->board[i][j]->ro, 0xFF, 0xFF, 0xFF, 0xFF);
				renderObjectLoadTextureRect(new_board->board[i][j]->ro, 99, 99);
				renderObjectSetPos(new_board->board[i][j]->ro, j * rect_offset, i * rect_offset);
			}
		}
	}
	
	return new_board;
}


static bool tttBoardDestroy(TTTBoard board)
{
	if (NULL != board) {
		if (NULL != board->circle) {
			renderObjectDestroy(board->circle);
			board->circle = NULL;
		}
		
		if (NULL != board->cross) {
			renderObjectDestroy(board->cross);
			board->cross = NULL;
		}
		
		for (int i = 0; i < board->dim.w; ++i) {
			for (int j = 0; j < board->dim.w; ++j) {
				tttFieldDestroy(board->board[i][j]);
				board->board[i][j] = NULL;
			}
		}
		
		for (int i = 0; i < board->dim.w; ++i) {
			free(board->board[i]);
		}
		
		free(board->board);
		
		free(board);
	}
}


static bool tttFieldSetOwner(TTTField field, int owner)
{
	bool ret = true;
	
	if (-1 == field->owner)
		field->owner = owner;
	else
		ret = false;
	
	return ret;
}


static bool tttFieldSetOverlay(TTTBoard board, TTTField field)
{
	bool ret = true;
	
	// only set if no overlay is present
	if (!field->overlay) {
		if (0 == field->owner)
			field->overlay = renderObjectCombine(field->ro, board->cross);
		else if (1 == field->owner)
			field->overlay = renderObjectCombine(field->ro, board->circle);
		else
			ret = false;
	} else {
		ret = false;
	}
	
	return ret;
}


static bool tttBoardReset(TTTBoard board)
{
	board->reset = false;
	
	int rect_offset = 100;
	
	for (int i = 0; i < board->dim.w; ++i) {
		for (int j = 0; j < board->dim.w; ++j) {
			board->board[i][j]->owner = -1;
			
			tttFieldDestroy(board->board[i][j]);
			
			board->board[i][j] = tttFieldCreate(board->rem->renderer);
			
			if (NULL == board->board[i][j])
				fprintf(stderr, "Failed to create board field!\n");
			
			renderObjectSetColor(board->board[i][j]->ro, 0xFF, 0xFF, 0xFF, 0xFF);
			
			renderObjectLoadTextureRect(board->board[i][j]->ro, 99, 99);
			
			renderObjectSetPos(board->board[i][j]->ro, j * rect_offset, i * rect_offset);
		}
	}
	
	board->turn = 0;
	board->winner = -1;
}



// helper functions
static int tttRowEqual(TTTField field[], size_t size)
{
	bool equal = true;
	int ret = -1;
	
	for (int i = 0; i < size - 1; ++i) {
		if (field[i]->owner != field[i+1]->owner) {
			equal = false;
			break;
		}
	}
	
	if (equal)
		ret = field[0]->owner;
	
	return ret;
}


static int tttColEqual(TTTField **field, int j, size_t size)
{
	bool equal = true;
	int ret = -1;
	
	for (int i = 0; i < size - 1; ++i) {
		if (field[i][j]->owner != field[i+1][j]->owner) {
			equal = false;
			break;
		}
	}
	
	if (equal)
		ret = field[0][j]->owner;
	
	return ret;
}


static void tttPrintOwnerMap(TTTBoard board)
{
	for (int i = 0; i < board->dim.w; ++i) {
		for (int j = 0; j < board->dim.w; ++j) {
			printf("%s ", board->board[i][j]->owner == 0 ? "X" : (board->board[i][j]->owner == 1 ? "O" : "-"));
		}
		
		printf("\n");
	}
}