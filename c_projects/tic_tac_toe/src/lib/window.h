#pragma once

#include <stdbool.h>

#include <SDL.h>


typedef SDL_Renderer *Renderer;

struct window {
	SDL_Window *window;
	const char *title;
	
	int x;
	int y;
	int width;
	int height;
	
	Uint32 flags;
};

typedef struct window *Window;



Window windowCreate(const char *title, int x, int y, int width, int height, Uint32 flags);


bool windowDestroy(Window window);