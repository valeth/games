#pragma once

#include "list.h"

#include <stdbool.h>


enum GRAPH_TYPE {
	GRAPH_UNDIRECTED,
	GRAPH_DIRECTED
};

typedef enum GRAPH_TYPE GraphType;


struct vertex {
	void *data;
	int index;
	
	bool visited;
	List connections;
};

typedef struct vertex *Vertex;



struct graph {
	GraphType type;
	List adj_list;
};

typedef struct graph *Graph;



static Vertex graphVertexCreate(int id, void *data);


static bool graphVertexDestroy(Vertex vertex);


static Vertex graphVertexGet(List list, int id);



Graph graphCreate(int size, GraphType type);


bool graphDestroy(Graph graph);


bool graphAddVertex(Graph graph, int id, void *data);


bool graphAddEdge(Graph graph, int v_id, int w_id);


static bool graphVerticesConnected(List path, Vertex src, Vertex dest);


bool graphIsConnected(Graph graph, int v_id, int w_id);


List graphGetConnectionPath(Graph graph, int v_id, int w_id);


bool graphSetData(Graph graph, int v_id, void *data);


void *graphGetData(Graph graph, int id);