#pragma once 

#include "graph.h"
#include "rendermanager.h"

#include <stdbool.h>

#include <SDL.h>



#define STATE_OBJECT			\
	int id; 			\
	EventHandlerFunc events; 	\
	LogicHandlerFunc logic; 	\
	RenderHandlerFunc render; 	\


struct statehandler;
typedef struct statehandler *StateHandler;

typedef int (*EventHandlerFunc)(StateHandler sth, SDL_Event event);
typedef int (*LogicHandlerFunc)(StateHandler sth);
typedef int (*RenderHandlerFunc)(StateHandler sth, RenderManager rem);

/*! \brief pointer to function that puts the states onto the graph
 *  \param sth the state handler
 *  \return number of states
 */
typedef bool (*StateInit)(StateHandler sth);

struct state {
	STATE_OBJECT
};

typedef struct state *State;


struct statehandler {
	State current;
	State prev;
	State request;
	Graph states;
};



/*! \brief create a new state handler object
 *  \param init the init function used to create the states
 *  \return the state handler or NULL on error
 */
StateHandler sthCreate();


/*! \brief delete a state handler
 *  \param sth the state handler
 *  \return true if successful, false if not
 */
bool sthDestroy(StateHandler sth);


/*! \brief request a state change
 *  \param sth the state handler
 *  \param state the requested state
 *  \return true if successful, false if not
 */
bool sthChangeState(StateHandler sth, int state);


/*! \brief try to set a requested state
 *  \param sth the state handler
 *  \return true if state change was successful, false if not
 */
bool sthSetState(StateHandler sth);


int sthGetState(StateHandler sth);


bool sthCurrentRender(StateHandler sth, RenderManager rem);

bool sthCurrentEvents(StateHandler sth, SDL_Event event);

bool sthCurrentLogic(StateHandler sth);

bool sthNewState(StateHandler sth, int id, EventHandlerFunc events, LogicHandlerFunc logic, RenderHandlerFunc render);

bool sthAddState(StateHandler sth, int id, State state);