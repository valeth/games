#include "app.h"

#include <stdio.h>
#include <stdlib.h>

#include <SDL_image.h>
#include <SDL_ttf.h>
#include <SDL_mixer.h>



App appCreate(const char *title, int w, int h)
{
	bool success = false;
	App new_app = malloc(sizeof(*new_app));

	
	if (!new_app) {
		success = false;
	} else {
		new_app->title = title;
		success = true;
	}
	
		
	if (success) {
		if (0 < SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO)) {
			fprintf(stderr, "SDL could not initialize! SDL Error: %s\n", SDL_GetError());
			success = false;
		}
	}
		
	if (success) {
		int img_flags = IMG_INIT_PNG;
		if (!(IMG_Init(img_flags) & img_flags)) {
			fprintf(stderr, "SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError());
			success = false;
		}
	}
		
	if (success) {
		if (0 > Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048)) {
			fprintf(stderr, "SDL_mixer could not initialize! SDL_mixer Error: %s\n", Mix_GetError());
			success = false;
		}
	}
		
	if (success) {
		if (-1 == TTF_Init()) {
			fprintf(stderr, "SDL_ttf could not initialize! SDL_ttf Error: %s\n", TTF_GetError());
			success = false;
		}
	}
		
		
	if (!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1"))
		fprintf(stderr, "Warning: Linear texture filtering not enabled!");

	if (success) {	
		new_app->window = windowCreate(new_app->title, -1, -1, w, h, SDL_WINDOW_SHOWN);
		
		if (!new_app->window)
			success = false;
	}
		
	// create the render manager
	if (success) {
		new_app->rem = rendermanagerCreate(new_app->window, SDL_RENDERER_ACCELERATED);
		
		if (!new_app->rem)
			success = false;
	}
	
	// create state handler
	if (success) {
		new_app->sth = sthCreate();
		
		if (!new_app->sth)
			success = false;
	}
	
	// set quit flag if any error occured during initialisation
// 	new_app->quit = !success;
	
	return new_app;
}


bool appDestroy(App app)
{

	if (NULL != app) {
		if (app->rem) {
			rendermanagerDestroy(app->rem);
			app->rem = NULL;
		}
		
		if (app->window) {
			windowDestroy(app->window);
			app->window = NULL;
		}
		
		free(app);
		
		TTF_Quit();
		Mix_Quit();
		IMG_Quit();
		SDL_Quit();
	}
}


int appRun(App app, int argc, char **argv)
{
	StateHandler sth = app->sth;
	
	while (!app->quit) {
		sthCurrentEvents(sth, app->event);
		sthCurrentLogic(sth);
		sthCurrentRender(sth, app->rem);
		
		sthSetState(sth);
		
		int cur_state = sthGetState(sth);
		
		if (0 == cur_state)
			app->quit = true;
	}
	
	return 0;
}



// bool appStateInit(StateHandler sth)
// {
// 	bool success = true;
// 	
// 	// add quit state
// 
// 	
// 	if (!sthNewState(sth, APP_STATE_MAIN, &appEventHandler, &appLogicHandler, &appRenderHandler)) {
// 		fprintf(stderr, "Failed to set graph data!\n");
// 		success = false;
// 	}
// 	
// 	sthChangeState(sth, APP_STATE_MAIN);
// 	sthSetState(sth);
// 	
// 	return success;
// }


// handlers for the state machine
int appEventHandler(StateHandler sth, SDL_Event event)
{
	while (SDL_PollEvent(&event)) {
		switch (event.type) {
		case SDL_QUIT:
			sthChangeState(sth, 0);
			break;
		}
	}
	
	return 0;
}


int appRenderHandler(StateHandler sth, RenderManager rem)
{
	rendermanagerRender(rem);
}


int appLogicHandler(StateHandler sth)
{
}