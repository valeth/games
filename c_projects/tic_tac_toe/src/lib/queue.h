/*!
 * \file queue.h
 * \author Patrick Auernig
 * \date 08.09.2015
 * \brief queue implemented with linked list
 */

#pragma once

#include "list.h"

#include <stdbool.h>



/*! \brief the queue */
struct queue {
	unsigned int max;	//!< the maximum number of elements in the queue
	unsigned int start;	//!< the start index
	unsigned int end;	//!< the end index
	bool full;		//!< full flag
	
	List queue;		//!< the list representing the queue
};

/*! \brief pointer to a queue */
typedef struct queue *Queue;


/*! \brief create a new queue
 *  \param size maximal size of the queue
 *  \return a queue object or NULL on error
 */
Queue queueCreate(unsigned int size);


/*! \brief delete a queue
 *  \param queue the queue object to delete
 *  \return true if successful, false if not
 */
bool queueDestroy(Queue queue);


/*! \brief check if a queue is empty
 *  \param queue the queue to check
 *  \return true if empty, false if not
 */
bool queueIsEmpty(Queue queue);


/*! \brief check if a queue is full
 *  \param queue the queue to check
 *  \return true if full, false if not
 */
bool queueIsFull(Queue queue);


/*! \brief put a new element onto the queue
 *  \param queue the target queue
 *  \param data  the data to put on
 *  \return true if successful, false if not
 */
bool queuePut(Queue queue, void *data);

/*! \brief take a element out of the queue
 *  \param queue the target queue
 *  \return the data element taken from the queue
 */
void *queueGet(Queue queue);


/*! \brief get the first element from the queue without removing it
 *  \param queue the target queue
 *  \return the first data element from the queue
 */
void *queueFront(Queue queue);