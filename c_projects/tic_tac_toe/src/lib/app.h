/*!
 * \file app.h
 * \author Patrick Auernig
 * \date 08.09.2015
 * \brief application
 * 
 * \see https://github.com/valeth/tic_tac_toe
 */

#pragma once

#include "window.h"
#include "rendermanager.h"
#include "statemachine.h"

#include <stdbool.h>

#include <SDL.h>



/*! \brief object that contains application information */
struct app {
	Window window; 			//!< the application window
	RenderManager rem;
	StateHandler sth;
	
	SDL_Event event;		//!< the SDL event queue

	bool quit;			//!< quit flag
	
	const char *title;		//!< the window title
};

/*! \brief a pointer to an app object */
typedef struct app *App;



/*! \brief create a new app object
 *  \param title the window title
 *  \param w     the window width
 *  \param h     the window height
 *  \return a new app object
 */
App appCreate(const char *title, int w, int h);


/*! \brief delete an app object
 *  \param app the object to delete
 *  \return true if successful, false if not
 */
bool appDestroy(App app);


/*! \brief render everything currently on the render queue
 *  \param app the appl object that contains the render queue
 *  \return true if successful, false if not
 */
bool appRender(App app);


int appRun(App app, int argc, char **argv);



bool appStateInit(StateHandler sth);

int appEventHandler(StateHandler sth, SDL_Event event);
int appRenderHandler(StateHandler sth, RenderManager rem);
int appLogicHandler(StateHandler sth);