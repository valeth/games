#include "list.h"
#include "debug.h"

#include <stdio.h>
#include <stdlib.h>



static ListNode listNodeCreate()
{
	ListNode new_node = malloc(sizeof(*new_node));
	
	if (new_node) {
		new_node->prev = NULL;
		new_node->next = NULL;
		new_node->data = NULL;
	}
	
	return new_node;
}


static bool listNodeClear(ListNode node)
{
	bool success = true;
	
	if (node && node->data)
		free(node->data);
	else
		success = false;
	
	return success;
}


static bool listNodeDestroy(ListNode node)
{
	bool success = true;
	
	if(node)
		free(node);
	else
		success = false;
	
	return success;
}


static bool listAdd(List list, void *data, int pos)
{
	bool success = true;
	
	if (!data || !list) {
		success = false;
	}
	
	if (success) {
		ListNode new_node = listNodeCreate();
		
		new_node->data = data;
		
		if (!list->head && !list->tail) {
			new_node->prev = NULL;
			new_node->next = NULL;
			list->head = new_node;
			list->tail = new_node;
		} else if (-1 == pos) {
			new_node->prev = NULL;
			new_node->next = list->head;
			list->head->prev = new_node;
			list->head = new_node;
		} else if (list->size == pos) {
			new_node->prev = list->tail;
			new_node->next = NULL;
			list->tail->next = new_node;
			list->tail = new_node;
		}
		
		++list->size;
	}
	
	return success;
}


static void *listRemove(List list, int pos)
{
	bool success = true;
	void *tmp_data = NULL;

	// list is already empty
	if (list && (!list->head || !list->tail))
		success = false;
	
	if (success) {
		ListNode del_node = NULL;
		
		if (list->head == list->tail) {
			del_node = list->head;
			list->head = NULL;
			list->tail = NULL;
		} else if (-1 == pos) {
			del_node = list->head;
			list->head = list->head->next;
			list->head->prev = NULL;
		} else if (list->size == pos) {
			del_node = list->tail;
			list->tail = list->tail->prev;
			list->tail->next = NULL;
		}
		
		tmp_data = del_node->data;
		free(del_node);
		
		--list->size;
	}
	
	return tmp_data;
}



List listCreate()
{
	List new_list = malloc(sizeof(*new_list));
	
	if (new_list) {
		new_list->size = 0;
		new_list->head = NULL;
		new_list->tail = NULL;
	} else {
		fprintf(stderr, "Failed to create list!\n");
	}
	
	return new_list;
}


bool listDestroy(List list)
{
	bool success = true;
	
	if (list) {
		LIST_FOREACH(list, current_node)
			listNodeDestroy(current_node->prev);
		
		if (list->tail)
			free(list->tail);
			
		list->tail = NULL;
		list->head = NULL;
		
		free(list);
	} else {
		success = false;
	}
	
	return success;
}


bool listClear(List list)
{
	bool success = true;
	
	if (list) {
		LIST_FOREACH(list, current_node)
			listNodeClear(current_node->prev);
		
		if (list->tail && list->tail->data)
			free(list->tail->data);
	} else {
		success = false;
	}
	
	return success;
}


bool listClearDestroy(List list)
{
	bool success = true;
	
	if (list) {
		success = listClear(list);
		
		if (success)
			success = listDestroy(list);
	} else {
		success = false;
	}
	
	return success;
}


bool listConcat(List list1, List list2)
{
	bool success = true;
	
	if (list1 && list2) {
		LIST_FOREACH(list2, current_node) {
			success = listPush(list1, current_node->data);
		}
	} else {
		success = false;
	}
	
	return success;
}


bool listPush(List list, void *data)
{
	return listAdd(list, data, list->size);

}


void *listPop(List list)
{
	return listRemove(list, list->size);
}


bool listUnshift(List list, void *data)
{
	return listAdd(list, data, -1);
}


void *listShift(List list)
{
	return listRemove(list, -1);
}


void *listGet(List list, unsigned int pos)
{
	void *ret_data = NULL;
	
	if (list) {
		int i = 0;
		LIST_FOREACH(list, current_node) {
			if (i == pos) {
				ret_data = current_node->data;
				break;
			}
			
			++i;
		}
	}
	
	return ret_data;
}