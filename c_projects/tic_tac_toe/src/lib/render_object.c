#include "render_object.h"

#include <stdio.h>
#include <stdlib.h>

#include <SDL_image.h>



RenderObject renderObjectCreate(Renderer renderer)
{
	RenderObject new_robj = NULL;
	new_robj = malloc(sizeof(*new_robj));
	
	if (renderer && new_robj) {
		new_robj->type = RENDER_OBJECT_TYPE_NONE;
		
		new_robj->texture = NULL;
		new_robj->dim.x = 0;
		new_robj->dim.y = 0;
		new_robj->dim.w = 0;
		new_robj->dim.h = 0;
		
		// default color key: cyan
		new_robj->color_key.r = 0x00;
		new_robj->color_key.g = 0xFF;
		new_robj->color_key.b = 0xFF;
		new_robj->color_key.a = 0xFF;
		
		// default draw color: black
		new_robj->color.r = 0x00;
		new_robj->color.g = 0x00;
		new_robj->color.b = 0x00;
		new_robj->color.a = 0xFF;
		
		new_robj->clip = NULL;
		new_robj->angle = 0.0;
		new_robj->center = NULL;
		new_robj->flip = SDL_FLIP_NONE;
		
		new_robj->renderer = renderer;
	} else {
		fprintf(stderr, "Failed to create new render object!\n");
	}
	
	return new_robj;
}

/* combine two render objects by rendering object b on top of a */
RenderObject renderObjectCombine(RenderObject a, RenderObject b)
{
	bool success = true;
	RenderObject new_robj = NULL;
	
	if (!a || !b)
		success = false;
	
	if (success) {
		new_robj = renderObjectCreate(a->renderer);
		
		if (!new_robj) {
			fprintf(stderr, "Failed to create new render object!\n");
			success = false;
		}
	}

	if (success) {
		// set the bigger dimension for the new render object
		if (a->dim.w >= b->dim.w)
			new_robj->dim.w = a->dim.w;
		else
			new_robj->dim.w = b->dim.w;
		
		if (a->dim.h >= b->dim.h)
			new_robj->dim.h = a->dim.h;
		else
			new_robj->dim.h = b->dim.h;
		
		new_robj->renderer = a->renderer;
	
		// use the position of a
		renderObjectSetPos(new_robj, a->dim.x, a->dim.y);
	
		new_robj->texture = SDL_CreateTexture(a->renderer,
						SDL_PIXELFORMAT_RGBA8888,
						SDL_TEXTUREACCESS_TARGET,
						new_robj->dim.w,
						new_robj->dim.h);
		
		if (!new_robj->texture) {
			renderObjectDestroy(new_robj);
			success = false;
		}
	}
	
	if (success) {
		// render to texture
		SDL_SetRenderTarget(new_robj->renderer, new_robj->texture);
	
		// clear the screen with the render color of a
		SDL_SetRenderDrawColor(new_robj->renderer, a->color.r, a->color.g, a->color.b, a->color.a);
		SDL_RenderClear(new_robj->renderer);
	
		SDL_RenderCopy(new_robj->renderer, a->texture, a->clip, &a->dim);
		SDL_RenderCopy(new_robj->renderer, b->texture, b->clip, &b->dim);
	
		// reset the render target
		SDL_SetRenderTarget(new_robj->renderer, NULL);
	
		new_robj->type = RENDER_OBJECT_TYPE_IMAGE;
	}
	
	return new_robj;
}

bool renderObjectDestroy(RenderObject ro)
{
	bool ret = true;
	
	if (ro) {
		if (NULL != ro->texture) {
			SDL_DestroyTexture(ro->texture);
			ro->texture = NULL;
		}

		free(ro);
	} else {
		ret = false;
	}
	
	return ret;
}


bool renderObjectLoadTextureFromFile(RenderObject ro, const char* filename)
{
	bool ret = true;
	
	if (ro) {
		if (ro->texture)
			SDL_DestroyTexture(ro->texture);
		
		SDL_Texture *new_texture = NULL;
		SDL_Surface *new_surface = IMG_Load(filename);
		
		if (new_surface) {
			SDL_SetColorKey(new_surface, SDL_TRUE,
					SDL_MapRGBA(new_surface->format, ro->color_key.r, ro->color_key.g, ro->color_key.b, ro->color_key.a));
			
			new_texture = SDL_CreateTextureFromSurface(ro->renderer, new_surface);
			
			if (new_texture) {

				ro->dim.w = new_surface->w;
				ro->dim.h = new_surface->h;
				ro->type = RENDER_OBJECT_TYPE_IMAGE;
			} else {
				fprintf(stderr, "Failed to create texture! SDL Error: %s\n", SDL_GetError());
				ret = false;
			}
			
			SDL_FreeSurface(new_surface);
		} else {
			fprintf(stderr, "Failed to create surface! SDL_image Error: %s\n", IMG_GetError());
			ret = false;
		}
		
		ro->texture = new_texture;
	} else {
		ret = false;
	}
	
	return ret;
}

bool renderObjectLoadTextureRect(RenderObject ro, int w, int h)
{
	bool ret = true;
	
	if (ro) {
		if (ro->texture)
			SDL_DestroyTexture(ro->texture);
	
		ro->texture = SDL_CreateTexture(ro->renderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, w, h);
		
		if (ro->texture) {
			ro->dim.w = w;
			ro->dim.h = h;
			
			// render to texture
			SDL_SetRenderTarget(ro->renderer, ro->texture);
			
			SDL_SetRenderDrawColor(ro->renderer, ro->color.r, ro->color.g, ro->color.b, ro->color.a);
			SDL_RenderClear(ro->renderer);
			SDL_RenderFillRect(ro->renderer, &ro->dim);
			
			// reset render target
			SDL_SetRenderDrawColor(ro->renderer, 0xFF, 0xFF, 0xFF, 0xFF);
			SDL_SetRenderTarget(ro->renderer, NULL);
			
			ro->type = RENDER_OBJECT_TYPE_GEOMETRY_RECTANGLE;
		} else {
			fprintf(stderr, "Failed to create blank texture! SDL Error: %s\n", SDL_GetError());
			ret = false;
		}
	} else {
		ret = false;
	}
	
	return ret;
}

bool renderObjectLoadTextureFromText(RenderObject ro, const char *text, TTF_Font *font)
{
	bool ret = true;
	
	if (ro) {
		if (ro->texture)
			SDL_DestroyTexture(ro->texture);
	
		SDL_Surface *text_surface = TTF_RenderUTF8_Blended(font, text, ro->color);
		
		if (text_surface) {
			SDL_SetRenderDrawColor(ro->renderer, ro->color.r, ro->color.g, ro->color.b, ro->color.a);
			SDL_RenderClear(ro->renderer);
			
			ro->texture = SDL_CreateTextureFromSurface(ro->renderer, text_surface);
			
			SDL_SetRenderDrawColor(ro->renderer, 0xFF, 0xFF, 0xFF, 0xFF);
			
			if (ro->texture) {
				ro->dim.w = text_surface->w;
				ro->dim.h = text_surface->h;
			} else {
				fprintf(stderr, "Failed to create texture from surface! SDL Error: %s\n", SDL_GetError());
				ret = false;
			}
			
			SDL_FreeSurface(text_surface);

		} else {
			fprintf(stderr, "Failed to create surface from rendered text! SDL_ttf Error: %s\n", TTF_GetError());
			ret = false;
		}
	} else {
		ret = false;
	}
	
	return ret;
}


bool renderObjectSetDim(RenderObject ro, int x, int y, int w, int h)
{
	bool ret = true;
	
	if (ro) {
		ro->dim.x = x;
		ro->dim.y = y;
		ro->dim.w = w;
		ro->dim.h = h;
	} else {
		ret = false;
	}
	
	return ret;
}

bool renderObjectSetPos(RenderObject ro, int x, int y)
{
	bool ret = true;
	
	if (ro) {
		ro->dim.x = x;
		ro->dim.y = y;
	} else {
		ret = false;
	}
	
	return ret;
}

bool renderObjectSetColorKey(RenderObject ro, Uint32 r, Uint32 g, Uint32 b, Uint32 a)
{
	bool ret = true;
	
	if (ro) {
		ro->color_key.r = r;
		ro->color_key.g = g;
		ro->color_key.b = b;
		ro->color_key.a = a;
	} else {
		ret = false;
	}
	
	return ret;
}

bool renderObjectSetColor(RenderObject ro, Uint32 r, Uint32 g, Uint32 b, Uint32 a)
{
	bool ret = true;
	
	if (ro) {

		ro->color.r = r;
		ro->color.g = g;
		ro->color.b = b;
		ro->color.a = a;
	} else {
		ret = false;
	}
	
	return ret;
}