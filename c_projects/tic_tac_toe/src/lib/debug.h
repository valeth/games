/* This file contains various macros for debugging purposes */

#pragma once


#include "log.h"
#include "check.h"


#ifdef NDEBUG
	#define log_debug(M, ...)
#else
	#define log_debug(M, ...)			\
		print_log("DEBUG", M, ##__VA_ARGS__)
#endif


#define check_debug(A, M, ...)				\
	if (!(A)) {					\
		log_debug(M, ##__VA_ARGS__);		\
		errno = 0;				\
		goto error;				\
	}
