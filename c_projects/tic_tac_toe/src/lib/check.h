/******************
 * checker macros *
 ******************/

#pragma once

#include "log.h"


#define check(A, M, ...)				\
	if (!(A)) {					\
		log_err(M, ##__VA_ARGS__);		\
        	errno = 0;				\
		goto error;				\
	}

#define check_mem(A) check((A), "Out of memory.")

#define sentinel(M, ...)				\
{							\
	log_err(M, ##__VA_ARGS__);			\
	errno = 0;					\
	goto error;					\
}
