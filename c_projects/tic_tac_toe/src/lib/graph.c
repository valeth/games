#include "graph.h"
#include "debug.h"

#include <stdlib.h>
#include <stdio.h>



static Vertex graphVertexCreate(int id, void *data)
{
	Vertex new_vertex = malloc(sizeof(*new_vertex));
	
	if (new_vertex) {
		new_vertex->connections = listCreate();
		
		if (new_vertex->connections) {
			new_vertex->index = id;
			new_vertex->data = data;
			new_vertex->visited = false;
		} else {
			free(new_vertex);
			new_vertex = NULL;
		}
	}
	
	return new_vertex;
}


static bool graphVertexDestroy(Vertex vertex)
{
	bool success = true;
	
	if (vertex) {
		listDestroy(vertex->connections);
		vertex->connections = NULL;
		
		free(vertex);
	}
	
	return success;
}


static Vertex graphVertexGet(List list, int id)
{
	Vertex found_vertex = NULL;

	if (list) {
		LIST_FOREACH(list, current_node) {
			Vertex cur_vertex = (Vertex)current_node->data;
			
			if (cur_vertex->index == id) {
				found_vertex = cur_vertex;
				break;
			}
		}
	}
	
	return found_vertex;
}



Graph graphCreate(int size, GraphType type)
{
	Graph new_graph = malloc(sizeof(*new_graph));
	
	if (new_graph) {
		new_graph->type = type;
		new_graph->adj_list = listCreate();
		
		if (new_graph->adj_list)
			for (int i = 0; i < size; ++i)
				graphAddVertex(new_graph, i, NULL);
	} else {
		log_debug("Failed to create graph");
	}
	
	return new_graph;
}


bool graphDestroy(Graph graph)
{
	bool success = true;
	
	if (graph) {
		if (graph->adj_list) {
			LIST_FOREACH(graph->adj_list, current_node)
				graphVertexDestroy((Vertex)current_node->data);
				
			listDestroy(graph->adj_list);
		}
			
		free(graph);
	} else {
		success = false;
	}
	
	return success;
}


bool graphAddVertex(Graph graph, int id, void *data)
{
	bool success = true;
	
	/* check if vertex id is already in the list */
	if (id >= graph->adj_list->size) {
		Vertex new_vertex = graphVertexCreate(id, data);
		
		if (new_vertex) {
			success = listPush(graph->adj_list, new_vertex);
		} else {
			log_debug("Failed to create vertex");
			success = false;
		}
	} else {
		log_debug("Vertex with id %d already exits", id);
		success = false;
	}
	
	return success;
}


bool graphAddEdge(Graph graph, int v_id, int w_id)
{
	bool success = true;
	Vertex v_vertex = NULL;
	Vertex w_vertex = NULL;
	
	// check if the id can be in the list
	if (v_id >= graph->adj_list->size) {
		log_err("Vertex id %d not in graph!\n", v_id);
		success = false;
	}
	
	if (w_id >= graph->adj_list->size) {
		log_err("Vertex id %d not in graph!\n", w_id);
		success = false;
	}
	
	if (success) {
		v_vertex = graphVertexGet(graph->adj_list, v_id);
		w_vertex = graphVertexGet(graph->adj_list, w_id);
	}
		
	if (v_vertex && w_vertex) {
		if (!graphVertexGet(v_vertex->connections, w_id)) {
			success = listPush(v_vertex->connections, w_vertex);
		} else {
			log_debug("Failed to add connection: %d -> %d", v_id, w_id);
			success = false;
		}
		
		if (graph->type == GRAPH_UNDIRECTED) {
			if (!graphVertexGet(w_vertex->connections, v_id)) {
				success = listPush(w_vertex->connections, v_vertex);
			} else {
				log_debug("Failed to add connection: %d -> %d", w_id, v_id);
				success = false;
			}
		}
	} else {
		success = false;
	}
	
	return success;
}


static void graphResetVisitedStatus(List adj_list)
{
	LIST_FOREACH(adj_list, adj_node) {
		Vertex cur_vertex = (Vertex)adj_node->data;
		cur_vertex->visited = false;
	}
}


static bool graphVerticesConnected(List path, Vertex src, Vertex dest)
{
	// check if src connects to dest directly
	bool connects = graphVertexGet(src->connections, dest->index);
	
	if (!connects) {
		src->visited = true;
		
		// check if we can reach dest through any of our connections
		LIST_FOREACH (src->connections, conn) {
			Vertex next_vertex = (Vertex)conn->data;
			
			if (!next_vertex->visited)
				connects = graphVerticesConnected(path, next_vertex, dest);	
			
			if (connects) {

				
				break;
			}
		}
	} if (path) {
		listUnshift(path, src);
	}
	
	return connects;
}


List graphGetConnectionPath(Graph graph, int v_id, int w_id)
{
	bool success = true;
	
	Vertex v_vertex = graphVertexGet(graph->adj_list, v_id);
	Vertex w_vertex = graphVertexGet(graph->adj_list, w_id);
	
	List path = listCreate();
	
	if (path) {
		success = graphVerticesConnected(path, v_vertex, w_vertex);
	} else {
		log_err("Failed to create list!");
		success = false;
	}
	
	if (!success) {
		log_debug("Vertices not connected");
	} else {
		listPush(path, w_vertex);
	}
	
	return path;
}


bool graphIsConnected(Graph graph, int v_id, int w_id)
{
	Vertex v_vertex = graphVertexGet(graph->adj_list, v_id);
	Vertex w_vertex = graphVertexGet(graph->adj_list, w_id);
	
	bool success = graphVerticesConnected(NULL, v_vertex, w_vertex);
	
	graphResetVisitedStatus(graph->adj_list);
	
	return success;
}


bool graphSetData(Graph graph, int v_id, void *data)
{
	bool success = true;
	
	Vertex set_vertex = graphVertexGet(graph->adj_list, v_id);
	
	if (set_vertex) {
		set_vertex->data = data;
	} else {
		log_err("Failed to set data for vertex %d", v_id);
		success = false;
	}
	
	return success;
}

void *graphGetData(Graph graph, int id)
{
	Vertex tmp_vertex = graphVertexGet(graph->adj_list, id);
	
	return tmp_vertex->data;
}