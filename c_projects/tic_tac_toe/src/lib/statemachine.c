#include "statemachine.h"
#include "app.h"
#include "debug.h"



StateHandler sthCreate()
{
	bool success = true;
	StateHandler new_sth = malloc(sizeof(*new_sth));
	
	if (!new_sth) {
		fprintf(stderr, "Failed to create state handler!\n");
		success = false;
	} else {
		new_sth->current = NULL;
		new_sth->request = NULL;
	}
	
	if (success) {
		new_sth->states = graphCreate(0, GRAPH_DIRECTED);
		
		if (!new_sth->states) {
			fprintf(stderr, "Failed to create graph!\n");
			success = false;
		}
	}
	
	if (success) {
		if (!sthNewState(new_sth, 0, NULL, NULL, NULL)) {
			fprintf(stderr, "Failed to create quit state!\n");
			success = false;
		}
	}
	
	if (!success) {
		sthDestroy(new_sth);
		new_sth = NULL;
	}
	
	return new_sth;
}


bool sthDestroy(StateHandler sth)
{
	bool success = true;
	
	if (sth) {
		if (sth->states) {
			graphDestroy(sth->states);
			sth->states = NULL;
		}
	} else {
		success = false;
	}
	
	return success;
}


bool sthChangeState(StateHandler sth, int state)
{
	State quit_state = graphGetData(sth->states, 0);
	
	// do not request a new state if the user is quitting
	if (sth->request != quit_state) {
		log_debug("Requesting state %d", state);
		sth->request = graphGetData(sth->states, state);
	} else {
		log_debug("Quit state set, ignoring request");
	}
		
	return (NULL != sth->request);
}


bool sthSetState(StateHandler sth)
{
	State quit_state = graphGetData(sth->states, 0);
	
	if (sth->request) {
		if (sth->current != quit_state) {
			sth->prev = sth->current;
			sth->current = sth->request;
			sth->request = NULL;
			log_debug("Set new state to %d", sth->current->id);
		}
	}
}


int sthGetState(StateHandler sth)
{
	if (sth && sth->current)
		return sth->current->id;
}


bool sthCurrentRender(StateHandler sth, RenderManager rem)
{
	bool success = true;
	
	if (sth && sth->current && sth->current->render)
		sth->current->render(sth, rem);
	else
		success = false;
	
	return success;
}


bool sthCurrentEvents(StateHandler sth, SDL_Event event)
{
	bool success = true;
	
	if (sth && sth->current && sth->current->events)
		sth->current->events(sth, event);
	else
		success = false;
	
	return success;
}


bool sthCurrentLogic(StateHandler sth)
{
	bool success = true;
	
	if (sth && sth->current && sth->current->logic)
		sth->current->logic(sth);
	else
		success = false;
	
	return success;
}


bool sthNewState(StateHandler sth, int id, EventHandlerFunc events, LogicHandlerFunc logic, RenderHandlerFunc render)
{
	bool success = true;
	State new_state = malloc(sizeof(*new_state));
	
	if (new_state) {
		new_state->id = id;
		new_state->events = events;
		new_state->logic = logic;
		new_state->render = render;
		
		if (!graphAddVertex(sth->states, id, new_state)) {
			fprintf(stderr, "Failed to add new state to graph!\n");
			free(new_state);
			success = false;
		}
	} else {
		fprintf(stderr, "Failed to create new state!\n");
		success = false;
	}
	
	return success;
}


bool sthAddState(StateHandler sth, int id, State state)
{
	return graphAddVertex(sth->states, id, state);
}