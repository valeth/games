#include "rendermanager.h"

#include <stdlib.h>
#include <stdio.h>



RenderManager rendermanagerCreate(Window window, Uint32 flags)
{
	bool success = true;
	RenderManager new_rem = malloc(sizeof(*new_rem));
	
	if (!new_rem) {
		fprintf(stderr, "Failed to create renderer manager\n");
		success = false;
	}
	
	if (success) {
		new_rem->renderer = SDL_CreateRenderer(window->window, -1, flags);
		
		if (!new_rem->renderer) {
			fprintf(stderr, "Failed to create renderer, SDL Error: %s\n", SDL_GetError());
			success = false;
		}
	}
	
	if (success) {
		new_rem->queue = queueCreate(256);
				
		if (!new_rem->queue) {
			fprintf(stderr, "Failed to create render queue!\n");
			success = false;
		}
	}
	
	if (!success) {
		rendermanagerDestroy(new_rem);
		new_rem = NULL;
	}
	
	return new_rem;
}


bool rendermanagerDestroy(RenderManager rem)
{
	bool success = true;
	
	if (rem) {
		if (rem->renderer) {
			SDL_DestroyRenderer(rem->renderer);
			rem->renderer = NULL;
		}
		
		if (rem->queue) {
			queueDestroy(rem->queue);
			rem->queue = NULL;
		}
		
		free(rem);
	} else {
		success = false;
	}
	
	return success;
}


bool rendermanagerEnqueue(RenderManager rem, RenderObject robj)
{
	return queuePut(rem->queue, robj);
}


bool rendermanagerRender(RenderManager rem)
{
	SDL_SetRenderDrawColor(rem->renderer, 0x00, 0x00, 0x00, 0xFF);
	SDL_RenderClear(rem->renderer);
	
	
	while (!queueIsEmpty(rem->queue)) {
		RenderObject robj = (RenderObject)queueGet(rem->queue);
		
		if (NULL == robj) {
			fprintf(stderr, "Failed to get render object!\n");
		} else {
			SDL_RenderCopyEx(rem->renderer, robj->texture, robj->clip, &robj->dim,
					robj->angle, robj->center, robj->flip);
		}
	}

	SDL_RenderPresent(rem->renderer);
}