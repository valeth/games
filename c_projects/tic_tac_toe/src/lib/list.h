/*!
 * \file list.h
 * \author Patrick Auernig
 * \date 08.09.2015
 * \brief linked list
 * 
 * \see https://github.com/valeth/tic_tac_toe
 */

#pragma once

#include <stdbool.h>



/*! \brief a list element */
struct list_node {
	void *data;		//!< pointer to data stored in the list
	
	struct list_node *next;	//!< pointer to the next node
	struct list_node *prev; //!< pointer to the previous node
};

/*! \brief pointer to a list_node object */
typedef struct list_node *ListNode;


/*! \brief a linked list */
struct list {
	unsigned int size;	//!< current size of the list
	
	ListNode head;		//!< pointer to the first element
	ListNode tail;		//!< pointer to the last element
};


/*! \brief pointer to a list object */
typedef struct list *List;



/*! \brief iterate through a list */
#define LIST_FOREACH(LIST, CUR_NODE)     \
	for(ListNode CUR_NODE = LIST->head; NULL != CUR_NODE; CUR_NODE = CUR_NODE->next)
		
	    
	    
/*! \brief Create a new list node
 *  \return ListNode or NULL on error
 */
static ListNode listNodeCreate();


/*! \brief Clear data field from a node
 *  \param node the list node to clear
 */
static bool listNodeClear(ListNode node);


/*! \brief Destroy a new list node
 *  \param node the list node to destroy
 */
static bool listNodeDestroy(ListNode node);


/*! \brief add an element into the list
 *  \param list the list to insert to
 *  \param data data pointer to insert
 *  \param pos  insert after position
 *  \return true if successful, false if not
 */
static bool listAdd(List list, void *data, int pos);


/*! \brief remove an element at from the list
 *  \param list the list to remove from
 *  \param pos the element to remove
 *  \return data pointer of removed node
 */
static void *listRemove(List list, int pos);



/*! \brief create a new linked list
 *  \return list or NULL on error
 */
List listCreate();


/*! \brief destroy a linked list
 *  \param list the list to destroy
 *  \return true if successful, false if not
 */
bool listDestroy(List list);


/*! \brief free data elements from every list node 
 *  \param list the list to clear
 *  \return true if successful, false if not
 */
bool listClear(List list);


/*! \brief clear and destroy a linked list
 *  \param list the list to destroy
 *  \return true if successful, false if not
 */
bool listClearDestroy(List list);


/*! \brief concatenate two linked lists
 *  \param dest the target list
 *  \param src the source list
 *  \return true if successful, false if not
 */
bool listConcat(List dest, List src);


/*! \brief add an element at the end of the list
 *  \param list the list to add to
 *  \param data the data to add
 *  \return true if successful, false if not
 */
bool listPush(List list, void *data);


/*! \brief remove the last element from a list
 *  \param list the list to remove from
 *  \return pointer to removed data
 */
void *listPop(List list);


/*! \brief insert at the beginning of the list
 *  \param list the list to unshift
 *  \param data pointer to data that should be added
 *  \return true if successful, false if not
 */
bool listUnshift(List list, void *data);


/*! \brief remove a data element from the beginning of the list
 *  \param list the list to remove data from
 *  \return pointer to removed data element
 */
void *listShift(List list);


/*! \brief get data from node without removing it
 *  \param list the list to query
 *  \param pos the node position from which to get data from
 *  \return data pointer or NULL on error
 */
void *listGet(List list, unsigned int pos);